#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# 파일명: trash-empty-timedelta-50000.py

# Gmail 휴지통에 쌓여있는 메시지들을 영구히 비우는 코드입니다.
# *** 실제로 영구히 삭제가 되기에 주의해서 테스트하시길 당부드립니다 ***
# *** 진짜로 영구히 삭제됩니다 주의해서 본 코드를 테스트하시길 다시금 부탁드립니다 ***
# 
# 대량의 메시지가 쌓여있는걸 비우려할때에는 시스템에러를 내뱉으며 팅깁니다.
#  그래서 기간내의 메시지들만 지우되 한번에 최대 5만개 까지만 지울 수 있도록
#  코드를 조금 수정했습니다.
# 좀 더 우아하게 기간내의 메시지들을 검색했을때 지워야 할 메시지의 갯수가 5만개를 넘어가면
#  5만개 이후의 나머지 메시지들도 함께 삭제할 수 있는 기능은 구현에 실패했습니다.
# 
# 본 코드는 슬프게도 파이썬 2.x 에서만 돌아갑니다;;;

# 원본 소스코드 출처: (본 코드는 제가 짠게 아닙니다. 주워온 코드로 짜집기했습니다.)
# https://stackoverflow.com/questions/12556649/increasing-speed-of-imap-bulk-message-deletion-in-python
# 도움 주신분: 우분투 한국 대화방의 autowiz님, jgwak님, 2019년 2월 15일-16일.

import datetime
import imaplib

m = imaplib.IMAP4_SSL("imap.gmail.com")  # Gmail 의 IMAP 서버 주소
print "Connecting to mailbox..."
m.login('GMAIL_ID', 'GMAIL_PASSWORD') # 본인의 Gmail 계정 정보를 입력합니다

print m.select('[Gmail]/Trash') # Gmail 휴지통을 선택합니다.
# 주의사항: Gmail 언어설정이 한국어로 되어있으면 작동하지 않습니다. 영어로 변경합니다.

before_date = (datetime.date.today() - datetime.timedelta(365)).strftime("%d-%b-%Y")
typ, data = m.search(None, '(BEFORE {0})'.format(before_date))
# Gmail 휴지통에서 지정한 기간내의 모든 메시지들을 찾아서 리스트에 집어넣습니다.
# datetime.timedelta(숫자) 를 조정하면서 코드를 반복 실행하면 단계적으로 지워나갈 수 있습니다.
# 예제로 쓰인 365 는 365일 즉 1년을 의미합니다.

if data != ['']:  # if not empty list means messages exist
    s_no_msgs = data[0].split()[-1]  # last msg id in the list
    if int(s_no_msgs) > 50000:
        no_msgs = 50000 # 한번에 5만개 정도까지는 버팁니다;;;
    else:
        no_msgs = s_no_msgs
        
    print "To be removed:\t", no_msgs, "messages found with date before", before_date
    m.store("1:{0}".format(no_msgs), '+FLAGS', '\\Deleted')  # delete forever (영구히 삭제함)
    print "Deleted {0} messages. Closing connection & logging out.".format(no_msgs)
else:
    print "Nothing to remove."

#This block empties trash, remove if you want to keep, Gmail auto purges trash after 30 days.
print("Emptying Trash & Expunge...")
m.expunge()  # not need if auto-expunge enabled
print("Done. Closing connection & logging out.")

m.close()
m.logout()
print "All Done."

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 2월 18일
