// FizzBuzz

package main

import "fmt"

func main() {
	for i := 1; i <= 100; i++ {
		switch {
		case i%3 == 0:
			fmt.Println("gopher")
		case i%8 == 0:
			fmt.Println("gogogo")
		default:
			fmt.Println(i)
		}
	}
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 15일