/* -*- coding: utf-8 -*- */

#include <stdio.h>

int main()
{
	char ustr[4][7] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	}; /* char 자료형으로 UTF-8 한글 처리 */

	printf("%s\n", ustr[0]);
	printf("%s\n", ustr[1]);
	printf("%s\n", ustr[2]);
	printf("%s\n", ustr[3]);

	printf("%p %s\n", &ustr, "ustr");
	printf("%p %s\n", &ustr[0], "ustr[0]");
	printf("%p %s\n", &ustr[1], "ustr[1]");
	printf("%p %s\n", &ustr[2], "ustr[2]");
	printf("%p %s\n", &ustr[3], "ustr[3]");
	
	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 최초 작성일: 2020년 6월 3일
 * 마지막 갱신: 2021년 1월 9일
 */
