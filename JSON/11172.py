# -*- coding: utf-8 -*-

import json

DPATH = "11172.json" # 변경 가능

f = open(DPATH, "r")
data = f.read(); f.close()

"""
https://stackoverflow.com/questions/5508509/how-do-i-check-if-a-string-is-valid-json-in-python
"""
def check_format(myjson):
    try:
        json_object = json.loads(myjson)
    except valueError as e:
        return False
    return True

if check_format(data) == True:
    dj = json.loads(data)
    KEY = dj.keys()
    VALUE = dj.values()
    ALL = dj.items()
    print("good,", len(ALL))
else:
    exit("wrong format!!!")

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 6일
