/* -*- coding: utf-8 -*- */

#include <stdio.h>

/* 출처: 
 * https://divestudy.tistory.com/8
 * https://norux.me/31
 */

int main()
{
	char str[4] = "가";

	printf("%x, %x, %x, %x\n", str[0], str[1], str[2], str[3]);
	printf("%d, %d, %d, %d\n", str[0], str[1], str[2], str[3]);

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 6월 5일
 */
