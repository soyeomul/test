/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

int cal(int n)
{
	return n + 1;
}

int main(int argc, char **argv)
{
	int i = atoi(argv[1]);

	printf("%d\n", cal(i)); /* 이곳이 핵심 */

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 6월 6일
 */
