/* -*- coding: utf-8 -*- */

#include <stdio.h>

/*
 * ### 문자열 한글 검사 ###
 * 참고문헌: https://cholol.tistory.com/383
 */

int main()
{
	int _mask = 0x80; /* 0b10000000 */
	char stk[] = "가";
	char ste[] = "abc";
	
	if (stk[0] & _mask) {
		printf("stk[0] ===> 한글 ok!\n");
	} /* 한글 */
	
	if (ste[0] & _mask) {
		printf("ste[0] ===> 한글 ok!\n");
	} /* 영문 알파벳 */
		
	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 6월 4일
 */
