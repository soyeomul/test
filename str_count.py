# -*- coding: utf-8 -*-

# 문자열에 각 알파벳 수를 세어서 출력하기
# ===> a:1 b:1 c:3 d:2 f:6

str = "a b c c c d d f f f f f f"
str_l = sorted(str.split())

str_c = []
for v in str_l:
    str_c.append("{0}:{1}".format(v, str_l.count(v)))

result = " ".join(sorted(list(set(str_c))))

print(result)
print(type(result))

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 5일
