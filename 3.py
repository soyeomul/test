#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 참고문헌: https://kldp.org/comment/630681#comment-630681

import subprocess

p = subprocess.Popen(["ps", "-ef"], stdout=subprocess.PIPE, universal_newlines=True)
for proc in p.stdout:
    if '?' not in proc:
        print(proc.strip())

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 5월 12일

"""
(bionic)soyeomul@localhost:~/test$ ./3.py
UID        PID  PPID  C STIME TTY          TIME CMD
root       370   334  0  5월11 pts/1  00:00:00 /sbin/agetty - 9600 xterm
root       374   334  0  5월11 pts/2  00:00:00 /sbin/agetty - 9600 xterm
root       379   334  0  5월11 pts/3  00:00:00 /sbin/agetty - 9600 xterm
soyeomul  3531  1346  0  5월11 pts/0  00:00:00 /bin/bash /usr/bin/crosh
soyeomul  3680  3531  0  5월11 pts/0  00:00:00 /bin/bash /usr/bin/crosh
soyeomul  3681  3680  0  5월11 pts/0  00:00:00 /bin/bash -l
soyeomul  8372  8366  0 22:14 pts/4    00:00:00 bash
soyeomul  9891  8372  0 22:23 pts/4    00:00:00 python3 ./3.py
soyeomul  9892  9891  0 22:23 pts/4    00:00:00 ps -ef
root     31581  3681  0 21:42 pts/0    00:00:00 sudo LD_LIBRARY_PATH=/usr/local/lib startgnome
root     31586 31581  0 21:42 pts/0    00:00:00 sh -e /usr/local/bin/enter-chroot -t gnome  exec startgnome
root     31889 31586  0 21:42 pts/0    00:00:00 su -s /bin/sh -c export SHELL='/bin/bash';'exec' 'startgnome'  - soyeomul
(bionic)soyeomul@localhost:~/test$ python3 --version
Python 3.6.7
(bionic)soyeomul@localhost:~/test$ 
"""
