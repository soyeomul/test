#!/usr/bin/env ruby2.5
# -*- coding: utf-8 -*-

# 리스트의 각 숫자를 정수형으로 변환 후 총합을 구하는 방법 예제

FPATH = "/home/soyeomul/hanwoo/007jch.txt"
a = `grep @1901 #{FPATH} | grep -v '^$' | awk -F '|' '{print $2}' | sed -e 's/\s//g'`

aa = a.split()
# 문자형으로 리스트 만들어짐 

int_aa = aa.map(&:to_i)
# 리스트내의 모든 원소를 문자형에서 정수형으로 변환하기

sum = 0
int_aa.each { |i| sum+=i }
# 합을 구하는 최종 절차

puts sum
# 결과값 출력
