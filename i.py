#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://opentutorials.org/course/2991/17975 (2의 보수와 음수)

x = 0b0011
y = 0b0101

z = x - y
print(bin(z), z) # -0b10 -2

#편집: Emacs 26.2 (Ubuntu 18.04)
#마지막 갱신: 2019년 7월 13일 

"""
y의 1의 보수는 비트 역위.
y의 2의 보수는 (1의 보수) + 1

x - y
=> 1110 == 0011 + 1010 + 0001
=> -(10000 - 1110)
=> -0010
=> -10(2진수)
"""
