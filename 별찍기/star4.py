#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

i = int(sys.argv[1])

_mark_m = "🐃" # U+1F403 (GNU)
_mark_p = "🐧" # U+1F427 (LINUX)

for j in range(i, 0, -2):
    m = i - j; m2 = m // 2 
    print(m2 * _mark_m, j * _mark_p, m2 * _mark_m, sep = "")

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 18일
