#include <stdio.h>
#include <string.h> /* strcat() */
#include <stdlib.h> /* atoi() */

char star[] = "🐂"; /* 한우 */
char gubun[999][999] = { NULL, };

void _sc(int *x, int *y);

int main(int argc, char **argv)
{
        int i;
        int input = atoi(argv[1]);

        for (i=0; i<input; i++) {
                _sc(&i, &i);
        }

        for (i=0; i<input; i++) {
                printf("%s \n", gubun[i]);
        }

        return 0;
}

void _sc(int *x, int *y)
{
        int i;
        for (i=0; i<*y; i++) {
                strcat(gubun[*x], star);
        }
}
       
/* 별찍기: 소(한우) */
