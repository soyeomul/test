#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

"""
(bionic)soyeomul@localhost:~/gitlab/test/별찍기$ ./remember0416.py 23
🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗^🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗^^^🎗🎗🎗🎗🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗🎗🎗🎗🎗^^^^^🎗🎗🎗🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗🎗🎗🎗^^^^^^^🎗🎗🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗🎗🎗^^^^^^^^^🎗🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗🎗^^^^^^^^^^^🎗🎗🎗🎗🎗🎗
🎗🎗🎗🎗🎗^^^^^^^^^^^^^🎗🎗🎗🎗🎗
🎗🎗🎗🎗^^^^^^^^^^^^^^^🎗🎗🎗🎗
🎗🎗🎗^^^^^^^^^^^^^^^^^🎗🎗🎗
🎗🎗^^^^^^^^^^^^^^^^^^^🎗🎗
🎗^^^^^^^^^^^^^^^^^^^^^🎗
^^^^^^^^^^^^^^^^^^^^^^^
(bionic)soyeomul@localhost:~/gitlab/test/별찍기$ 
"""

i = int(sys.argv[1])

_mark_m = "🎗" # U+1F397 (Remember 0416)
_mark_p = "%s" % ("^") # __the_Roots_for_Universe__

for j in range(1, i+1, 2):
    m = i - j; m2 = m // 2
    print(m2 * _mark_m, j * _mark_p, m2 * _mark_m, sep = "")

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 4월 16일
