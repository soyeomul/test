#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# 파일명: trash-empty-timedelta.py

# Gmail 휴지통에 쌓여있는 메시지들을 영구히 비우는 코드입니다.
# 기간을 지정해서 해당 기간내에 있는 메시지들을 비웁니다.
# 휴지통에 대량(10만개 이상)의 메시지가 쌓여있을경우에는,
#  한번에 휴지통을 비우려할때에 시스템에러를 내뱉으며 팅기기에
#  기간을 정해서 단계적으로 지우게끔 산법(알고리즘)을 수정하는게 좋을거 같다는 위즈님의 조언이 있었습니다.
#
# 본 코드는 파이썬 2.x 에서만 돌아갑니다.

# 원본 소스코드 출처: (본 코드는 제가 짠게 아닙니다. 인터넷 여기저기서 주워온 코드로 짜집기했습니다.)
# https://stackoverflow.com/questions/12556649/increasing-speed-of-imap-bulk-message-deletion-in-python
# 도움 주신분: 우분투 한국 대화방의 autowiz님, jgwak님, 2019년 2월 15일.

import datetime
import imaplib

m = imaplib.IMAP4_SSL("imap.gmail.com")  # Gmail 의 IMAP 서버 주소
print "Connecting to mailbox..."
m.login('GMAIL_ID', 'GMAIL_PASSWORD')

print m.select('[Gmail]/Trash') # Gmail 의 휴지통을 선택합니다.
# 주의사항: Gmail 언어설정이 한국어로 되어있으면 작동하지 않습니다. 영어로 변경합니다.

before_date = (datetime.date.today() - datetime.timedelta(365)).strftime("%d-%b-%Y")
typ, data = m.search(None, '(BEFORE {0})'.format(before_date))
# Gmail 의 휴지통에서 지정한 기간내의 모든 메시지들을 찾아서 리스트에 집어넣습니다.
# datetime.timedelta(숫자) 를 조정하면서 코드를 반복 실행하면 단계적으로 지워나갈 수 있습니다.

if data != ['']:  # if not empty list means messages exist
    no_msgs = data[0].split()[-1]  # last msg id in the list
    print "To be removed:\t", no_msgs, "messages found with date before", before_date
    m.store("1:{0}".format(no_msgs), '+FLAGS', '\\Deleted')  # delete forever
    print "Deleted {0} messages. Closing connection & logging out.".format(no_msgs)
else:
    print "Nothing to remove."

#This block empties trash, remove if you want to keep, Gmail auto purges trash after 30 days.
print("Emptying Trash & Expunge...")
m.expunge()  # not need if auto-expunge enabled
print("Done. Closing connection & logging out.")

m.close()
m.logout()
print "All Done."

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 2월 16일

'''
# 테스트 해보니 5만개씩 정도는 시스템에러 없이 지워집니다.
(bionic)soyeomul@localhost:~/work$ ./trash-empty-timedelta.py
Connecting to mailbox...
('OK', ['277703'])
To be removed:	58080 messages found with date before ......
Deleted 58080 messages. Closing connection & logging out.
Emptying Trash & Expunge...
Done. Closing connection & logging out.
All Done.
(bionic)soyeomul@localhost:~/work$ ./trash-count.rb
219623
(bionic)soyeomul@localhost:~/work$ 
'''
