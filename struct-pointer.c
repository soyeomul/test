/* -*- coding: utf-8 -*- */

/*
 * 구조체 포인터 예제
 * 
 * 참고문헌: 
 * [1] <https://modoocode.com/55>
 * [2] 포인터의 이해: <초보자를 위한 리눅스 C프로그래밍 ISBN 9-788980-544-127>
 */

#include <stdio.h>

struct _test {
	int asdf;
	char *comment;
};

int main(void)
{
	struct _test t[3];
	struct _test *pt;

	pt = t; /* pt = &t[0]; */

	pt->asdf = 0; 
	pt->comment = "첫번째 배열 t[0]";

	pt++; /* 다음 배열로 넘김 */

	pt->asdf = 1;
	pt->comment = "두번째 배열 t[1]";

	pt++; /* 다음 배열로 넘김 */

	pt->asdf = 2;
	pt->comment = "세번째 배열 t[2]";
	
	printf("%d %s \n", t[0].asdf, t[0].comment);
	printf("%d %s \n", t[1].asdf, t[1].comment);
	printf("%d %s \n", t[2].asdf, t[2].comment);
	
	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 11일
 */
