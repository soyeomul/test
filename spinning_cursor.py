# -*- coding: utf-8 -*-
# https://irclogs.ubuntu.com/2019/05/30/%23ubuntu-ko.html (서니님의 spinning_cursor 조언)
# https://stackoverflow.com/questions/48854567/python-asynchronous-progress-spinner

# 테스트: Python 3.6.7, Ubuntu 18.04 

import sys, time, threading
import subprocess

def spin_cursor():
    while True:
        for cursor in '|/-\\':
            sys.stdout.write(cursor)
            sys.stdout.flush()
            time.sleep(0.3) # adjust this to change the speed
            sys.stdout.write('\b')
            if done:
                return

# start the spinner in a separate thread
done = False
spin_thread = threading.Thread(target=spin_cursor)
spin_thread.start()

# do some more work in the main thread, or just sleep:
#time.sleep(10)
print("내려받는중 ...")
subprocess.call("rm -f 20160327_114518.hex*; wget -q https://gitlab.com/soyeomul/Gnus/raw/master/__system__/20160327_114518.hexdata", shell=True)

# tell the spinner to stop, and wait for it to do so;
# this will clear the last cursor before the program moves on
done = True
spin_thread.join()

# continue with other tasks
sys.stdout.write("All done.\n")
