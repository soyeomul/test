# -*- coding: utf-8 -*-

# Author: Manwoo Yi <https://github.com/navilera>
# https://kldp.org/node/164259

def half_adder(a, b):
    s = a ^ b
    c = a & b

    return s, c
 

def adder(a, b):
    s = 0
    c = 0
    for i in range(32):
        hs, hc = half_adder((a>>i)&1, (b>>i)&1)
        ts, tc = half_adder(hs, c)
        c = hc | tc
        s |= ts << i
       
    return s


if __name__ == "__main__":
    import sys
    
    a = int(sys.argv[1])
    b = int(sys.argv[2])

    print(adder(a, b))


# VIM (Chrome OS), 2021-04-28
