#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IPv6/IPv4

x6 = 16 ** 4
y6 = x6 ** 8

x = 2 ** 8
y = x ** 4

print("IPv4:", y)
print("IPv6:", y6)

xyz = y6 // y

v = format(xyz, ".2E")

print("v6/v4:", v)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 12일

"""
IPv4: 4294967296
IPv6: 340282366920938463463374607431768211456
v6/v4: 7.92E+28
"""
