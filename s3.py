#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 우분투 한국 대화방 (#ubuntu-ko) 원격 grep 도구
# (1) 하루치 로그 검색 가능
# (2) 한달치 로그 검색 가능
# (3) 결과행의 날짜 태그 적용
# (4) 영문일경우 대소문자 구분없이 검색 가능
# (5) 결과행에서 검색어 쉽게 식별할 수 있도록 밑줄 표시
# (6) UTF-8 에러나는 로그도 검색할 수 있도록 코드 보완
# (7) 커서 순환/반복: 지루함 방지용
# (8) 찾을문자열에 공백이 있으면 OR 연산을 하도록 설정함
# (9) 존재하지않는 URL 을 표시하도록 설정함
 
# 실행환경:
# 파이썬 3
# 가급적 리눅스/*BSD 시스템에서 실행할것

# 도움주신분: 서니님(Seony)
# 20190622 -- 순환문에 관한 화두를 던져주셨음 
# 20190530 -- 커서 순환/반복에 관한 조언을 주셨음

import sys
import time
import threading
import re
from urllib.request import urlopen
from datetime import datetime
from subprocess import Popen, PIPE

def make_durl(xyz): # 하루치 로그
    year = xyz[0:4]
    month = xyz[4:6]
    day = xyz[6:8]

    FURL = []
    FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
        .format(year, month, day))

    return FURL


def make_murl(xyz): # 한달치 로그
    year = xyz[0:4]
    month = xyz[4:6]

    ds = list(range(1,32))

    rs = []
    for x in ds:
        rs.append(str(x).zfill(2))

    FURL = []
    for day in rs:
        FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
                    .format(year, month, day))

    return FURL


_help = """\
사용법: %s 찾을문자열 찾을년월일(또는 찾을년월)
예시(찾을년월일): %s 안녕하세요 20190126
예시(찾을년월): %s 안녕하세요 201901""" % (
    sys.argv[0],
    sys.argv[0],
    sys.argv[0],
)

if len(sys.argv) < 2 or sys.argv[1] == "--help":
    sys.exit(_help)

if len(sys.argv[2]) == 6: # 한달치 로그
    FURL = make_murl(sys.argv[2])
elif len(sys.argv[2]) == 8: # 하루치 로그
    FURL = make_durl(sys.argv[2])
    
class colors:
    """
    POSIX 시스템 터미널에서 글자에 색깔 적용하기: 
    https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
    """
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"

# 
# 찾을 문자열 -- 검색어
# 
def _key(xyz):
    if " " in xyz:
        r = xyz.split()
    else:
        r = xyz

    return r


#
# 검색된 문장 재작성
#
def _sr(k, l):
    l = l.lower()

    if "list" in str(type(k)):
        for i in range(len(k)):
            k[i] = k[i].lower() 
            if k[i] in l:
                l = l.replace(k[i], "{0}{1}{2}"\
                              .format(colors.UNDERLINE,
                                      k[i],
                                      colors.ENDC,))
    else:
        k = k.lower()
        if k in l:
            l = l.replace(k, "{0}{1}{2}"\
                          .format(colors.UNDERLINE,
                                  k,
                                  colors.ENDC,))

    return l
            

#
# 날짜 태그
#
def _dt(xyz):
    exp = re.compile("com/([0-9][0-9][0-9][0-9])/([0-9][0-9])/([0-9][0-9])/")

    chop1 = exp.search(xyz).group(1)
    chop2 = exp.search(xyz).group(2)
    chop3 = exp.search(xyz).group(3)

    r = chop1 + chop2 + chop3

    return r


def get_text(xyz):
    try:
        req = urlopen(xyz)
        text = req.read().decode("utf-8", "replace")
    except:
        return ""

    return text


def get_grep(url):
    output = get_text(url)
    if len(output) == 0:
        print("\b", "$ cat /dev/null > %s" % (url))
        return

    data = output.splitlines(False)
    fdate = _dt(url) # 검색된 문장의 날짜 태그
    
    key = _key(sys.argv[1])
    
    for i in range(len(data)):
        result_ = _sr(key, data[i])
        if len(result_) == len(data[i]):
            sys.stdout.write("\b")
        else:
            print("\b", fdate, result_)

                
start_time = datetime.now() # 시간 측정 시작.
print("===> %s 기간 에서 %s 를(을) 검색합니다" % (
    sys.argv[2],
    sys.argv[1],
))
print("... 검색중 ... 커피한잔과 함께,,, 천천히 기다려주세요 ...^^;;;")

# 지루함 방지용: 커서 순환/반복 ...
"""
Spinner taken from:
https://stackoverflow.com/questions/48854567/python-asynchronous-progress-spinner
"""
def spin_cursor():
    while True:
        for cursor in "-\\|/":
            sys.stdout.write(cursor)
            sys.stdout.flush()
            time.sleep(0.3) # 속도 조절
            sys.stdout.write("\b")
            if done:
                return

            
# start the spinner in a separate thread
done = False
spin_thread = threading.Thread(target=spin_cursor)
spin_thread.start()

# 커서 순환/반복할 동안 작업할 목록 아래에 기록:      
for karma in FURL:
    get_grep(karma)

# tell the spinner to stop, and wait for it to do so;
# this will clear the last cursor before the program moves on
done = True
spin_thread.join()

# 커서 순환/반복은 끝났지만, 다음 할일을 아래에다 기록:
time_elapsed = datetime.now() - start_time # 시간 측정 끝.
print("# 검색시간(hh:mm:ss.ms): %s" % (time_elapsed))

sys.stdout.write("^고맙습니다 _布德天下_ 감사합니다_^))//\n")

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2021년 5월 4일
