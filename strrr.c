/* -*- coding: utf-8 -*- */

#include <stdio.h>

/* 젠장할 문자열 실험 */

int main(void)
{
	char *str = "KOREA";
        /* char str[6] = { 'K', 'O', 'R', 'E', 'A', '\0', }; */

	printf("%p %p %p \n", &str[0], &*(str), str);
	printf("%c %c %c \n", str[0], str[1], str[2]);
	printf("%c %c %c \n", *(str), *(str+1), *(str+2));
	printf("%c %c %c \n", *str, *str+1, *str+2);
	printf("%s \n", str);

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 12월 1일
 */
