/* -*- coding: utf-8 -*- */
 
/* 함수 문자열 포인터배열 연습 */


#include <stdio.h>

void _print(char *xyz[], int idx)
{
	int i;
	
	for (i=0; i<idx; i++) {
		printf("%s \n", xyz[i]);
	}
}

int main(void)
{
	char *asdf[4]  =  {
		"봄",
		"여름",
		"가을",
		"겨울",
	};

	_print(asdf, 4);

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 10월 23일 
 */
