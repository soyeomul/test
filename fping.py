#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import PIPE, Popen
from os import path
import sys

"""
REFERENCE:
<https://mail.python.org/pipermail/python-list/2022-February/thread.html#905156>
"""

if not path.isfile('/usr/bin/fping'):
    sys.exit("you first install fping, then try again...")
    
def check(xyz):
    cmd = "fping %s" % (xyz)
    try_ = Popen(cmd, stdout=PIPE, shell=True)
    output = try_.communicate()[0].decode("utf-8")

    return output


if __name__ == "__main__":
    xyz = sys.argv[1]
    if "alive" in check(xyz):
        print("ok")
    else:
        print("something is wrong...")

# 2022-02-27, GNU Emacs 27.1 (Debian 11 Bullseye)
