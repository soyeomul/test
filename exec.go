// 명령어 실행
//
// 참고문헌:
// <https://stackoverflow.com/questions/54269243/golang-equivalent-of-creating-a-subprocess-in-python/54269689>

package main

import "os/exec"

func main() {
	furl := "https://gitlab.com/soyeomul/hanwoo/-/raw/master/LICENSE"

	cmd := exec.Command(
		"curl",
		"-o",
		"hanwoo-license.txt",
		furl,
	)
	cmd.Start()
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 16일
