# -*- coding: utf-8 -*-
# 이것은 저의 첫 파이썬 코드입니다.

data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
print data*3

# 실행 결과:
# ~/python_ruby $ python --version
# Python 2.7.3
# ~/python_ruby $ ls -l thanks.py
# -rw-rw-r--   1 soyeomul       soyeomul      248  7월 23 21:56 thanks.py
# ~/python_ruby $ python thanks.py
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# ^고맙습니다 _地平天成_ 감사합니다_^))//
#
# ~/python_ruby $ 
# 
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 23일
# 마지막 갱신: 2017년 7월 24일
