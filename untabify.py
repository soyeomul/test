#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# tab replace to space 4
"""
Newsgroups: comp.lang.python
From: Gilmeh Serda <gilmeh.serdah@nothing.here.invalid>
Date: Sat, 30 Nov 2019 17:13:07 GMT
Message-ID: <DqxEF.113374$1S4.110719@fx10.am4>
"""

from sys import argv
from os import chdir, listdir, path

DIR = path.realpath(argv[-1])
chdir(DIR)
for FILE in [f for f in listdir(DIR) if f.endswith('.py')]:
    fut = open(path.join(DIR, FILE)).read().replace('\t', ' '*4)
    print(path.join(DIR, FILE))
    with open(path.splitext(path.join(DIR, FILE))[0]+'_new.py', 'w') as f:
        f.write(fut)

# 2019-12-04 (GNU Emacs 26.3 -- Ubuntu 18.04)

"""
(bionic)soyeomul@localhost:~$ bin2/untabify.py bin2
/home/soyeomul/bin2/getmail.py
/home/soyeomul/bin2/untabify.py
/home/soyeomul/bin2/thanks-mid.py
/home/soyeomul/bin2/gcp-tunnel.py
/home/soyeomul/bin2/cronstart.py
(bionic)soyeomul@localhost:~$ ls -l bin2
total 40
-rwxrwxr-x. 1 soyeomul soyeomul  154 10월 27 10:48 cronstart.py
-rw-rw-r--. 1 soyeomul soyeomul  154 12월  2 17:53 cronstart_new.py
-rwxrwxr-x. 1 soyeomul soyeomul  494 11월 30 21:39 gcp-tunnel.py
-rw-rw-r--. 1 soyeomul soyeomul  494 12월  2 17:53 gcp-tunnel_new.py
-rwxrwxr-x. 1 soyeomul soyeomul 1253 11월 11 11:50 getmail.py
-rw-rw-r--. 1 soyeomul soyeomul 1253 12월  2 17:53 getmail_new.py
-rwxrwxr-x. 1 soyeomul soyeomul 3845 11월 30 11:16 thanks-mid.py
-rw-rw-r--. 1 soyeomul soyeomul 3845 12월  2 17:53 thanks-mid_new.py
-rwxrwxr-x. 1 soyeomul soyeomul  653 12월  2 17:50 untabify.py
-rw-rw-r--. 1 soyeomul soyeomul  653 12월  2 17:53 untabify_new.py
(bionic)soyeomul@localhost:~$ 
"""
