/* -*- coding: utf-8 -*- */

#include <stdio.h>

struct _s {
	char comment[99];
};

int main(void)
{
	struct _s s[3] = { 0, }; /* 0 == '\0' */
	
	scanf("%s", s[0].comment);
	scanf("%s", s[2].comment);

	printf("%s\n%s\n%s\n", s[0].comment, s[1].comment, s[2].comment);

	
	return 0;
}
 
/* 
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 24일
 */
	
