# -*- coding: utf-8 -*-

def mf():
    return "my func!"

if __name__ == "__main__":
    print("직접 실행")
    print(__name__)
else:
    print("임포트 실행")
    print(__name__)
    
# 출처:
# https://medium.com/@chullino/if-name-main-%EC%9D%80-%EC%99%9C-%ED%95%84%EC%9A%94%ED%95%A0%EA%B9%8C-bc48cba7f720
# https://wikidocs.net/29
