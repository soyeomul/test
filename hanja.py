# -*- coding: utf-8 -*-

import re
import subprocess

# libhangul 한자 사전
# https://github.com/libhangul/libhangul/blob/master/data/hanja/hanja.txt

# 순수 한자 목록만 뽑아봅니다 (중복된 한자 제외) 
# *** 파이썬 3.7 필요합니다 ***
 
"""
# 참고문헌: (1-3) 
(1) https://ko.wikipedia.org/wiki/%ED%95%9C%EC%A4%91%EC%9D%BC_%ED%86%B5%ED%95%A9_%ED%95%9C%EC%9E%90#%ED%95%9C%EC%A4%91%EC%9D%BC_%ED%86%B5%ED%95%A9_%ED%95%9C%EC%9E%90
(2) https://www.phpschool.com/gnuboard4/bbs/board.php?bo_table=qna_function&wr_id=319281
(3) https://stackoverflow.com/questions/30069846/how-to-find-out-chinese-or-japanese-character-in-a-string-in-python/30070664
"""

# 유니코드 한자 범위: r00 ~ r10
r00 = "\U00004e00-\U00009fff" # (한중일 통합 한자)
r01 = "\U00003400-\U00004db5" # (한중일 통합 한자 확장 A)
r02 = "\U00020000-\U0002a6d6" # (한중일 통합 한자 확장 B)
r03 = "\U0002a700-\U0002b734" # (한중일 통합 한자 확장 C)
r04 = "\U0002b740-\U0002b81f" # (한중일 통합 한자 확장 D)
r05 = "\U0002b820-\U0002ceaf" # (한중일 통합 한자 확장 E)
r06 = "\U0002ceb0-\U0002ebe0" # (한중일 통합 한자 확장 F)
r07 = "\U0000f900-\U0000faff" # (한중일 호환용 한자)
r08 = "\U0002f800-\U0002fa1f" # (한중일 호환용 한자 보충)
r09 = "\U000031c0-\U000031ef" # (한중일 한자 획)
r10 = "\U00002ff0-\U00002fff" # (한자 생김꼴 지시 부호)

def get_hanja(xyz): # 한자를 뽑아내는 함수
    chop = re.match("[{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}]"\
                    .format(r00, r01, r02, r03, r04, r05, r06, r07, r08, r09, r10), xyz)
    
    if chop: # 일치되는 한자가 있으면 반환후 함수 탈출!
        return chop.group(0)
        
FURL = "https://raw.githubusercontent.com/libhangul/libhangul/master/data/hanja/hanja.txt"

p = subprocess.Popen("curl -s {}".format(FURL), \
                            stdout=subprocess.PIPE, text=True, shell=True)

list_hanja = [] 
while True:
    line = p.stdout.readline() # 파일용량이 크기에 한 행씩 분석해나감
    if not line: # 행이 끝나면 탈출!
        break
    for index in range(0, len(line)):
        hanja = get_hanja(line[index])
        if hanja: # 각 행에서 한글자씩 대조하여 한자가 검출되면 list_hanja 로 집어넣음
            list_hanja.append(hanja)

sorted_hanja = sorted(list(set(list_hanja))) # 정렬된 한자 목록

for idx in range(0, len(sorted_hanja)): # 색인/한자/유니코드식별번호 
    print(str(idx+1).zfill(5), sorted_hanja[idx], hex(ord(sorted_hanja[idx])).replace("0x", "u+").upper())

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 8월 1일
