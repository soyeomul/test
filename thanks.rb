# -*- coding: utf-8 -*-
# 이것은 저의 첫 루비 코드입니다.

data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
3.times { print data }

# 실행 결과:
# ~/python_ruby $ ruby --version
# ruby 1.8.7 (2011-06-30 patchlevel 352) [i686-linux]
# ~/python_ruby $ ls -l thanks.rb
# -rw-rw-r--   1 soyeomul       soyeomul      231  7월 23 21:28 thanks.rb
# ~/python_ruby $ ruby thanks.rb
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# ~/python_ruby $ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 23일
# 마지막 갱신: 2017년 7월 24일
