/* -*- coding: utf-8 -*- */

#include <stdio.h>

int main(void)
{
	int i;
	char *day[] = {
		"월요일",
		"화요일",
		"수요일",
		"목요일",
		"금요일",
		"토요일",
		"일요일",
	};

	for (i=0; i<7; i++) {
		printf("%s \n", day[i]);
	}

	return 0;
}

/* EOF */
