# -*- coding: utf-8 -*-
#
# 참고문헌:
# https://stackoverflow.com/questions/8381499/replace-words-in-string-ruby

$data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"

def thanks()
  v = $data.gsub "_地平天成_", "_布德天下_"
  return v
end
# 문자열 치환을 합니다.

3.times { print thanks() }
# 3번 반복시켜 화면에 뿌립니다.

# 실행 결과:
# (precise)soyeomul@localhost:~/python_ruby$ ruby --version
# ruby 1.8.7 (2011-06-30 patchlevel 352) [i686-linux]
# (precise)soyeomul@localhost:~/python_ruby$ ruby thanks-replace.rb
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 24일
# 마지막 갱신: 2017년 7월 24일

