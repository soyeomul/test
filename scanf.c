/* -*- coding: utf-8 -*- */

#include <stdio.h>

int main(void)
{
	char asdf[3];
	
	scanf("%c", &asdf[0]);
	/*
	 * asdf 가 char 배열일경우
	 * %s 로 받고
	 * scanf("%s", asdf);
	 * 처럼 주소연산자 생략 가능함
         *
         * 문자열은 정수형 타입 (int) 와 좀 다르게 취급됨
	 */

	printf("%c \n", asdf[0]);


	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 10월 21일
 */
