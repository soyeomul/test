/* -*- coding: utf-8 -*- */

/*
 * 구조체 포인터 함수 예제
 *
 * 참고문헌: <https://modoocode.com/60>
 */

#include <stdio.h>


struct _test {
	char *name;
	int age;
	int gender;
};

int set_human(struct _test *a, char *name, int age, int gender);

int main(void)
{
	struct _test human;

	set_human(&human, "소여물", 38, 1);

	printf("%s %d %d \n", human.name, human.age, human.gender);

	return 0;
}

int set_human(struct _test *a, char *name, int age, int gender)
{
	a->name = name;
	a->age = age;
	a->gender = gender;

	return 0;
}

/* __EOF__ */
