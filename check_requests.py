# coding: utf-8
#!/usr/bin/env python3
# require to install requests + rich

# Author: foxmask <https://gitlab.com/foxmask>

import argparse
import datetime
import requests
from rich.console import Console
from time import sleep
import sys

console = Console()


def go(nickname, date):

    date_range = []

    if len(date) == 6:
        year = date[0:4]
        month = date[5:6].zfill(2)

        for x in list(range(1,32)):
            date_range.append(str(x).zfill(2))
    else:
        date = datetime.datetime.strptime(date, "%Y%m%d")
        year = date.year
        month = str(date.month).zfill(2)
        day = str(date.day).zfill(2)
        date_range.append(day)

    logs = []

    for day in date_range:

        url = "https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt".format(year, month, day)

        req = requests.get(url)

        if req.status_code == 200:
            for line in req.text.splitlines():
                if nickname in line:
                    console.print(line)
                    logs.append(line)
        elif req.status_code == 404:
            console.print("no log found for that date {}{}{}".format(year, month, day), style="magenta")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("nickname",
                        type=str,
                        help="the nickname to search")
    parser.add_argument("date",
                        type=str,
                        help="to get logs the month give year and month eg 202104 to get all april")

    args = parser.parse_args()

    go(args.nickname, args.date)

