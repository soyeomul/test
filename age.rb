#!/usr/bin/ruby1.8
# -*- coding: utf-8 -*-

# 송아지 출생일자를 입력받아서 나이(개월령)를 출력해주는 코드입니다.

require "date"

exp = Regexp.new("^([0-9]{4}).*([0-9]{2}).*([0-9]{2})$")
print "출생일자를 입력하세요 (예: 2017-08-17): "
rs = gets.chomp.to_s

ix = exp.match(rs)[1]
iy = exp.match(rs)[2]
iz = exp.match(rs)[3]

x = ix.to_i

if iy[0].chr == '0'
  y = iy[1].chr.to_i
else
  y = iy.to_i
end

if iz[0].chr == '0'
  z = iz[1].chr.to_i
else
  z = iz.to_i
end

tday = DateTime.now
bday = DateTime.new(x, y, z, 0, 0, 0, 0.375) # x, y, z 는 정수형 타입
timedelta = tday - bday

age = timedelta.to_i

if age*12%365 < 15
  mage = age*12/365
else
  mage = age*12/365 + 1 # age in months (개월령)
end

puts mage.to_s + "개월령" + " " + "(" + rs + ")"

# 편집: Emacs 23.3 (Ubuntu 12.04)
# 마지막 갱신: 2018년 1월 25일
