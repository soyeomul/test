#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

n = int(sys.argv[1]) # 유효값 [1-255]

x = n >> 4 & 0xf # 상위 4비트
y = n & 0xf # 하위 4비트

z = hex(x)[2:] + hex(y)[2:] + "(16)"
print(z, hex(n), n)
