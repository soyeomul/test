#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import subprocess
 
today="2019-06-03"

p = subprocess.Popen("who | grep `echo {}`".format(today), stdout=subprocess.PIPE, universal_newlines=True, shell=True)
lst = p.stdout.readlines()
 
if len(lst) != 0:
    for v in lst:
        print(v)
else:
    try:
        subprocess.call("who", shell=True)
    except subprocess.CalledProcessError as e: # 서니님의 조언
        print(e)

print("lst:", len(lst)) # if/else 구문이 정확한가의 판별 기준

# 참고문헌: https://kldp.org/node/161668
# 실험 호스트: Ubuntu 18.04 (Google Compute Engine)
# 사용 파이썬: 3.6.7

# 편집: VIM - Vi IMproved 8.0 (2016 Sep 12, compiled Apr 10 2018 21:31:58)
# 마지막 갱신: 2019년 6월 3일
