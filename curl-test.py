#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 참고문헌:
# https://gitlab.com/soyeomul/test/-/commit/6527c883009d8d974f3d7a9f476d78e6b0cf21bc

import sys
import time
from subprocess import Popen, PIPE

MUA = """\
Mozilla/5.0 (X11; CrOS aarchb64 14092.46.0) \
AppleWebKit/537.36 (KHTML, like Gecko) \
Chrome/93.0.4577.69 Safari/537.36"""

TARGET = "https://httpbin.org/user-agent"

def _get_agent(xyz=None):
    try:
        if xyz:
            cmd = "curl -s -H 'User-Agent: %s' %s" % (xyz, TARGET)
        else:
            cmd = "curl -s %s" % (TARGET)
    except:
        pass
    
    _try = Popen(cmd, stdout=PIPE, shell=True)
    output = _try.communicate()[0].decode("utf-8").strip()

    return output


if __name__ == "__main__":
    print(_get_agent(MUA))
    time.sleep(3)
    sys.exit(_get_agent()) # 기본값
   
# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 9월 13일    
