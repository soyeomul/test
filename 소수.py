#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 소수의 정의: 1과 자신으로만 나누어지는 정수

# 소수 식별 일반 산법:
# 1과 자신 이외의 수로 나누어 떨어지면 소수가 *아님* ===> "False"

# 테스트 유효범위: 2이상의 양의 정수

from datetime import datetime
import sys

수행시작 = datetime.now()

def f(x):
    if x == 2:
        return x, "True"

    for i in range(2, x):
        if x % i == 0:
            return x, "False"

    return x, "True"

범위값 = int(sys.argv[1]) # 2부터 범위값까지 테스트

for 숫자 in range(2, 범위값+1):
    print(f(숫자))

수행완료 = datetime.now()
print("소요시간:", 수행완료 - 수행시작)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 3월 30일

# EOF
