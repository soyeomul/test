#include <stdio.h>

void swap(char *ch)
{
	*ch = '3';
}

int main()
{
	char c = 'a';
	printf("variable c = %c \n", c);

	swap(&c);
	printf("variable c = %c \n", c);

	return 0;
}
