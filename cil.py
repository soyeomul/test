# -*- coding: utf-8 -*-

# 빌드로그를 분석하여 각 파일이 설치된 경로를 파악해줍니다
# 문자열 "/usr/bin/install" 이 핵심입니다.

import subprocess
import sys
import os
import re

POS = os.name = "posix"
PY37K = sys.hexversion >= 0x03070000
FURL = "https://gitlab.com/soyeomul/stuff/raw/master/typescript.20190616"

if not POS:
    print("본 코드는 리눅스 또는 유닉스 시스템에서 돌아갑니다.")
    print("Operating System:", os.name)
    sys.exit(1)

if PY37K: 
    p = subprocess.Popen("curl -s {}".format(FURL), \
                         stdout=subprocess.PIPE, text=True, shell=True)
else:
    print("죄송합니다. 파이썬 3.7 이 필요합니다.")
    print("그리고 curl 이 깔려있는지도 확인해주세요.")
    print("현재 파이썬 판번호:", sys.version_info[0:3])
    sys.exit(1)

data_full = p.stdout.read()

data_full = data_full.replace("\\\n", "")

data_full = data_full.split("\n")

data = []
for x in data_full:
    if "/usr/bin/install -c" in x and "BSD-compatible" not in x:
        data.append(x.strip())

def replace_data(x):
    exp = re.compile("(.*)/usr/bin/install")
    chop = exp.search(x).group(1)
    x = x.replace(chop, "")
    
    return x

data = list(map(lambda xyz: replace_data(xyz), data))

def split_data(x):
    if ";" in x:
        x = " ".join(x.split()[0:6])
    else:
        x = x

    return x

data = list(map(lambda xyz: split_data(xyz), data))

data_libhangul = []
for x in data:
    if "git20180606" in x or "libhangul" in x:
        data_libhangul.append(x)

data_ibus = []
for x in data:
    if "git20180606" not in x and "libhangul" not in x:
        data_ibus.append(x)

### 분석 및 정렬된 최종 결과물을 화면에 출력합니다 ###
print("# libhangul 설치경로:")
for x in data_libhangul:
    print(x)

print("")

print("# ibus-hangul 설치경로:")
for x in data_ibus:
    print(x)

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 18일
