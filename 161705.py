# -*- coding: utf-8 -*-

# https://kldp.org/node/161705

f = open("161705.txt", "r")
data = f.readlines()

list_A = []
for x in data:
    if 'A' in x:
        list_A.append(int(x.split()[1].strip()))

list_B = []
for x in data:
    if 'B' in x:
        list_B.append(int(x.split()[1].strip()))

list_C = []
for x in data:
    if 'C' in x:
        list_C.append(int(x.split()[1].strip()))

print("A최소값:", min(list_A), '\t', "A최대값:", max(list_A))
print("B최소값:", min(list_B), '\t', "B최대값:", max(list_B))
print("C최소값:", min(list_C), '\t', "C최대값:", max(list_C))

f.close()

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 8일

"""
출력결과: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ cat 161705.txt
A 10
A 20
A 30
A 40
A 50
B 13
B 13
B 14
B 15
C 43
C 412
C 423
C 431
(bionic)soyeomul@localhost:~/test$ python3 161705.py
A최소값: 10 	 A최대값: 50
B최소값: 13 	 B최대값: 15
C최소값: 43 	 C최대값: 431
(bionic)soyeomul@localhost:~/test$ 
"""
