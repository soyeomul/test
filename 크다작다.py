# -*- coding: utf-8 -*-

import sys

lst = [0, 6, 8, 10,]

def 크다(x, y):
    if  x < y:
        return True


def 작다(x, y):
    if x > y:
        return True


if __name__ == "__main__":
    n = int(sys.argv[1])
    if n <= min(lst) or n >= max(lst):
        sys.exit("알림: 1부터 9까지의 양의정수만 유효합니다")
        
    큼 = []; 작음 = []
    for i in range(len(lst)):
        if 크다(lst[i], n):
            큼.append(lst[i])

    for j in range(len(lst)):
        if 작다(lst[j], n):
            작음.append(lst[j])

    print("%s 는 %s 와 %s 사이에 있는 숫자네유~~~" % (
        n,
        max(큼),
        min(작음),
    ))

# EOF
