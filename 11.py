#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 참고문헌: https://kldp.org/node/136970
# 각 줄의 앞문자열 3자를 문자형에서 정수형으로...
# 그리고 각 항목에다 5 를 더한후 출력...
"""
$ cat file.list
031-234-2345
131-234-2345
231-234-2345
331-234-2345
441-234-2345
$
"""

f = open("file.list", "r")
lines = f.readlines()

aa = []
for x in lines:
    aa.append(x[0:3])

for bb in aa:
    print(int(bb)+5) 

f.close()

"""
테스트: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ ./11.py
36
136
236
336
446
(bionic)soyeomul@localhost:~/test$ 
"""
