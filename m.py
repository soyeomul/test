#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://docs.python.org/3/library/re.html#re.Match.end

import re

str = "soyeomul@gremove_thismail.com"

p = re.compile("remove_this")
m = p.search(str)

email = str[:m.start()] + str[m.end():]

print(repr(email)) # 'soyeomul@gmail.com'

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 14일
