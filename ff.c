/* -*- coding: utf-8 -*- */

#include <stdio.h>

int hello(int n)
{
	n = 2;
}
	
int main(void)
{
	int n = 1;
	printf("%d \n", n);
	
	hello(n);
	printf("%d \n", n);

	/* 
	 * 그래서 주소로 접근해야 그 값을 바꿀 수 있다
         * 그냥 대입을 하면 다른 주소값이 생성된다 본코드처럼
         *
         * 포인터의 핵심은 주소값으로 가야지만 해당값을 변경할 수 있다는걸
         * 깊이 깊이 깨닫자
         *
         * 관련 포인터 코드 하단 주석으로 첨부함.
         */

	return 0;
}

/* 
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 10월 19일
 */

  
/*** 이것이 바로 주소로 접근하는 예시
#include <stdio.h>

int hello(int *n)
{
        *n = 2;
}

int main(void)
{
        int n = 1;
        printf("%d \n", n);

        hello(&n);
        printf("%d \n", n);

        return 0;
}
***/
