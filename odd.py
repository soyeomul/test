#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def _odd(xyz):
    return xyz % 2 == 1

for idx in range(1, 11):
    if _odd(idx) == 1:
        print(idx)

# EOF
