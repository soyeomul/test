#!/usr/bin/env python3
# -*- coding: utf-8 -*-


btable = (
    "01", "02", "03", "04", "05",
    "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15",
    "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25",
    "26", "27", "28", "29", "30",
    "31", "32", "33", "34", "35",
    "36", "37", "38", "39", "40",
    "41", "42", "43", "44", "45",
)


def dcheck(list):
    """ 로또 결과 중복 숫자 있는지 점검 """
    return len(list) == len(set(list))


def karmic_cycle(length):
    from time import time
    """
    random 모듈이 좀 무겁다는 생각에...
    저녁 소여물 주면서 계속 랜덤함수를 생각했어요
    끊임없이 변하는것을 변하지 않는것으로 나누면 좋겠다싶었어요
    변하는건 시간, 변하지않는건 공간 그래서 이 함수가 만들어졌어요

    부처님오신날이라 그런지
     이게 꼭 불교의 윤회(자연의 순환-반복하는 이치)처럼 느껴졌습니다
    --소여물, 2020년 4월 30일 (음력 4월 8일)
    """
    _constant = 108 * 361 * 13 # 시간을 좀 더 세분화시킴
    vtime = int(time() * _constant)

    return vtime % length


trandom = karmic_cycle


def eval():
    tn = trandom(len(btable))

    return int(btable[tn])


lotto = [] # 여기다 로또 숫자를 담습니다 


for idx in range(6):
    lotto.append(eval())


# 결과 출력
if dcheck(lotto):
    print(sorted(lotto))
else:
    print("로또 실패 !!!")


# 실험환경: Python 3.9.2 / 크롬북 (데비안 11 Bullseye)
# 만든날짜: 2022년 12월 17일
# 만든사람: 볏짚상하차 (규리-수아 아빠) 
