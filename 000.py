# -*- coding: utf-8 -*-

# 출력 양식 정렬 예제: 파이썬3 필요함
# 출처: <https://python.bakyeono.net/chapter-11-2.html>

countries = [
    {'name': 'China', 'population': 1403500365},
    {'name': 'Japan', 'population': 126056362},
    {'name': 'South Korea', 'population': 51736224},
    {'name': 'Pitcairn Islands', 'population': 56}
]

print(type(countries))

for v in countries:
    print(type(v), v)

print("------")

form = '나라: {0:>16} | 인구: {1:>10}'
for country in countries:
    print(form.format(country['name'], country['population']))

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 7일


