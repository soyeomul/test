#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 본 스크립트는 현재 디렉토리에서만 작동합니다.
# 각 파일의 첫번째 줄 또는 두번째줄로 파일명을 변경합니다.
# 참고문헌: https://kldp.org/node/161528

import subprocess
import re

# 현재 디렉토리내에 존재하는 파일들을 리스트로 집어넣기
# 제외: 하위디렉토리, 심볼릭링크, 실행파일
cmd_1st = "rm -f /tmp/ls.txt"
cmd_2nd = "ls -F | grep -v '/$' | grep -v '@$' | grep -v '*$' > /tmp/ls.txt"
subprocess.call(cmd_1st, shell=True)
subprocess.call(cmd_2nd, shell=True)
sf = open("/tmp/ls.txt", "r")

flist = sf.read().split()

sf.close()

# 파일명을 변경하는 함수
def rename_file(bf):
    f = open(bf, "r")
    
    exp = re.compile("\.(.*)$")
    
    if "." in str(bf):
        fe = "." + exp.search(bf).group(1) # 파일 확장자 추출하기
    else:
        fe = "" # 파일 확장자가 없는 경우

    lines = f.readlines()

    if lines: # 문자열이 존재하는 경우 
        af1 = lines[0].strip() + fe
        af2 = lines[1].strip() + fe
        af1 = af1.replace(" ", "") # 파일명 사이에 공백을 없애줌
        af2 = af2.replace(" ", "") # 파일명 사이에 공백을 없애줌
    else: # 두줄 모두다 문자열이 없을 경우
        af1 = fe
        af2 = fe

    if af1 == fe and af2 == fe: # 두줄 모두다 문자열이 없을 경우
        subprocess.call("echo '{}: sorry, there has no strings in lines.'".format(bf), shell=True)
    elif af1 == fe: # 첫번째 줄이 공백일 경우 
        subprocess.call("mv -v {0} {1}".format(bf, af2), shell=True) 
    else: # 첫번째 줄에 문자열 있음
        subprocess.call("mv -v {0} {1}".format(bf, af1), shell=True)

    f.close()

# for문으로 리스트의 모든 파일명을 변경하기 
for v in flist:
    rename_file(v)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 최초 작성일: 2019년 5월 6일
# 마지막 갱신: 2019년 5월 7일

"""
실행결과: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ ls -l
total 36
-rwxrwxr-x. 1 soyeomul soyeomul 4056  5월  7 21:53 1.py
-rwxrwxr-x. 1 soyeomul soyeomul 3472  5월  6 23:17 1.py~
-rw-rw-r--. 1 soyeomul soyeomul   32  5월  7 21:47 1.txt
-rw-rw-r--. 1 soyeomul soyeomul   36  5월  7 21:46 2.lst
-rwxrwxr-x. 1 soyeomul soyeomul  247  5월  7 20:56 2.py
-rwxrwxr-x. 1 soyeomul soyeomul  212  5월  6 21:27 2.py~
-rw-rw-r--. 1 soyeomul soyeomul   52  5월  7 21:48 3.xyz
-rw-rw-r--. 1 soyeomul soyeomul   23  5월  7 22:00 asdf
-rw-rw-r--. 1 soyeomul soyeomul    0  5월  7 20:42 empty-file
drwxrwxr-x. 2 soyeomul soyeomul 4096  5월  6 22:14 sss
lrwxrwxrwx. 1 soyeomul soyeomul   10  5월  7 21:45 zzz -> empty-file
(bionic)soyeomul@localhost:~/test$ ./1.py
renamed '1.txt' -> '안녕하세요.txt'
renamed '2.lst' -> '아름다운한글.lst'
renamed '3.xyz' -> '봄여름가을겨울.xyz'
renamed 'asdf' -> 'ThisIsAsdf'
empty-file: sorry, there has no strings in lines.
(bionic)soyeomul@localhost:~/test$ ls -l
total 36
-rwxrwxr-x. 1 soyeomul soyeomul 4056  5월  7 21:53 1.py
-rwxrwxr-x. 1 soyeomul soyeomul 3472  5월  6 23:17 1.py~
-rwxrwxr-x. 1 soyeomul soyeomul  247  5월  7 20:56 2.py
-rwxrwxr-x. 1 soyeomul soyeomul  212  5월  6 21:27 2.py~
-rw-rw-r--. 1 soyeomul soyeomul   23  5월  7 22:00 ThisIsAsdf
-rw-rw-r--. 1 soyeomul soyeomul    0  5월  7 20:42 empty-file
drwxrwxr-x. 2 soyeomul soyeomul 4096  5월  6 22:14 sss
lrwxrwxrwx. 1 soyeomul soyeomul   10  5월  7 21:45 zzz -> empty-file
-rw-rw-r--. 1 soyeomul soyeomul   52  5월  7 21:48 봄여름가을겨울.xyz
-rw-rw-r--. 1 soyeomul soyeomul   36  5월  7 21:46 아름다운한글.lst
-rw-rw-r--. 1 soyeomul soyeomul   32  5월  7 21:47 안녕하세요.txt
(bionic)soyeomul@localhost:~/test$ cat -n ThisIsAsdf
     1	This Is Asdf
     2	thanks^^^
(bionic)soyeomul@localhost:~/test$ cat -n 봄여름가을겨울.xyz
     1	
     2	봄 여름 가을 겨울
     3	금수강산 대한민국
(bionic)soyeomul@localhost:~/test$ cat -n 아름다운한글.lst
     1	아름다운 한글
     2	감사합니다
(bionic)soyeomul@localhost:~/test$ cat -n 안녕하세요.txt
     1	안녕하세요
     2	반갑습니다
(bionic)soyeomul@localhost:~/test$ cat -n empty-file
(bionic)soyeomul@localhost:~/test$ 
"""
