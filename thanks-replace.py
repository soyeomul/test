# -*- coding: utf-8 -*-
#
# 다음처럼 쉽게 문자열을 치환할 수 있어요~
#
# >>> a = "Life is too short"
# >>> a.replace("Life", "Your leg")
# 'Your leg is too short'
#
# 참고문헌: https://wikidocs.net/13 (문자열 바꾸기)

data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
# 기반이 되는 문자열을 data 에 저장합니다.

def thanks():
    v = data.replace("_地平天成_", "_布德天下_")
    return v
# 문자열 치환을 합니다.

print thanks()*3
# 3회 반복하여 화면에 뿌립니다.

# 실행 결과:
# (precise)soyeomul@localhost:~/python_ruby$ python --version
# Python 2.7.3
# (precise)soyeomul@localhost:~/python_ruby$ python thanks-replace.py
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
#
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 24일
# 마지막 갱신: 2017년 7월 24일
