;;; ~/.gnus
;;; -*- coding: utf-8 -*-

;;; 사용자 개별 모듈 
(add-to-list 'load-path (expand-file-name "~/.emacs.d/gnus"))


;;; ^_^))//
(setq user-mail-address "soyeomul@doraji.xyz")
(setq user-full-name "황병희") ;; 原始返本에 따라...
(setq message-from-style 'angles) ;; RFC2822
(setq sendmail-program "/usr/sbin/ssmtp")
(setq message-sendmail-extra-arguments '("-C" "/etc/ssmtp/ssmtp-yw.conf"))


;;; ^Gmane_^))//
;; https://lars.ingebrigtsen.no/2020/01/15/news-gmane-org-is-now-news-gmane-io/
(setq gnus-select-method '(nntp "news.gmane.io"))


;;; lkml and git
(setq gnus-secondary-select-methods
      '((nntp "lore"
	      (nntp-address "nntp.lore.kernel.org"))))


;;; This was the Usenet.
(add-to-list 'gnus-secondary-select-methods
	     '(nntp "september"
		    (nntp-address "news.eternal-september.org")))


(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "yw"
		      (nnimap-address "imap.gmail.com")
		      (nnimap-server-port "imaps")
		      (nnimap-stream ssl)))


(setq gnus-posting-styles
      '((".*"
	 ("X-Message-SMTP-Method" "sendmail")
	 (eval
	  (setq message-cite-style nil))
	 ("X-Operating-System" "≪ding≫ -- The Gnus <ding@gnus.org>")
         ("X-Gnus-Desktop" (shell-command-to-string "env.py"))
	 (organization "金陵 (연원의 마음)")
	 ("X-Thanks-Thanks-Thanks" "천하창생을 끝까지 다 살리시려는 마음...^^^")
	 ("X-Thanks-Right-Mindset" "'toothless' & 'hiccup'"))
	("gmane.emacs.gnus.general"
	 (name "Byung-Hee HWANG") ;; https://inbox.vuxu.org/ding/87y2ht9o8q.fsf@zoho.eu/ 
	 ("Face" nil)) ;; <<ding>> 메일링 서버는 총 헤더 길이에 제한을 둡니다 그래서...
	("gmane.mail.mutt.user"
	 (eval
	  (require 'my-mutt-style))
	 (eval
	  (setq message-cite-style 'message-cite-style-mutt))
	 (name "Byung-Hee HWANG"))
	((header "to" "cr.yp.to")
	 (name "Byung-Hee HWANG"))
	((header "cc" "cr.yp.to")
	 (name "Byung-Hee HWANG"))
	((header "from" "cr.yp.to")
	 (name "Byung-Hee HWANG"))
	((header "to" "soyeomul@yw.doraji.xyz")
	 ;; - 메일건 실험용 주소
	 ;; - emacs-humanities@gnu.org 가입 -- 2020-12-13
	 ("X-Message-SMTP-Method" "smtp penguin 2000")
	 (address "soyeomul@yw.doraji.xyz"))))


;;; Color: gnus-header-from (기본값: "red3")
;; 사람 이름은 검정 글씨로 쓰는거라고 학교에서 배웠어유;;;
(require 'gnus-art)
(set-face-foreground 'gnus-header-from "black")


;;; If you want to see gravatars, set:
(setq gnus-treat-from-gravatar 'head)
(setq gnus-treat-mail-gravatar 'head)
(setq gnus-gravatar-size 39)


;;; 보고 싶은 헤더들^^^
(setq gnus-visible-headers
      '("^From:"
	"^Newsgroups:"
	"^Subject:"
	"^Date:"
	"^To:"
	"^Cc:"
	"^Organization:"
	"^Mail-Followup-To:"
	"^In-Reply-To:"
	"^Reply-To:"
	"^Message-ID:"
	"^X-Newsreader:"
	"^User-Agent:"
	"^X-Mailer:"
	"^X-Envelope-From:"
	"^X-Google-Original-From:"
        "^X-Mailgun-Sending-Ip:"
	"^X-Gnus-Desktop:"
	"^X-Thanks-Right-Mindset:"
	"^X-Debian-Message:"
	"^X-Injected-Via-Gmane:"))


;;; 포워딩되어 도착한 GCP 메일들을 위한 조치
;; 답장할때에 받는이란에 함께 추가됨
(setq message-extra-wide-headers
      '("X-Google-Original-From"))


;;; ~~~ 이건 별로 중요하지 않은 옵션입니다 ~~~
;;; 그룹 버퍼에서 첫 메일 작성시 From 주소 선택
(require 'my-gnus-first-try)
(global-set-key (kbd "C-x <f6>") 'set-gcp-yw) ;; <soyeomul@yw.doraji.xyz>
(global-set-key (kbd "C-x <f7>") 'set-daum) ;; <byunghee.hwang@daum.net>
(global-set-key (kbd "C-x <f8>") 'set-gmail) ;; <soyeomul@gmail.com>
(global-set-key (kbd "C-x <f9>") 'set-yw) ;; <기본값>
;; 그룹 버퍼에서 `r' 을 누르면 Gnus 기본값을 읽어들입니다 


;;; 가입한 메일링 리스트 목록
(setq message-subscribed-addresses
      '("ding@gnus.org" ;; <soyeomul@doraji.xyz> <byunghee.hwang@daum.net>
	"mutt-users@mutt.org" ;; <soyeomul@doraji.xyz>
        "gce-discussion@googlegroups.com" ;; <soyeomul@gmail.com>
        "chromium-os-discuss@chromium.org" ;; <soyeomul@doraji.xyz>
        "info-gnus-english@gnu.org" ;; <soyeomul@doraji.xyz>
        "help-gnu-emacs@gnu.org" ;; <soyeomul@doraji.xyz> <soyeomul@__vladivostok__>
        "emacs-humanities@gnu.org" ;; <soyeomul@yw.doraji.xyz>
        "emacs-orgmode@gnu.org" ;; <soyeomul@doraji.xyz>
        "emacs-devel@gnu.org" ;; <soyeomul@doraji.xyz>
        "users@spamassassin.apache.org" ;; <byunghee.hwang@daum.net>
        "debian-user@lists.debian.org" ;; <soyeomul@doraji.xyz>
        "python-list@python.org" ;; <soyeomul@doraji.xyz>
        "ubuntu-users@lists.ubuntu.com" ;; <soyeomul@doraji.xyz>
        "questions@freebsd.org" ;; <soyeomul@doraji.xyz>
        "netbsd-users@netbsd.org" ;; <soyeomul@doraji.xyz>
        "mailman-users@python.org" ;; <soyeomul@doraji.xyz>
        "postfix-users@postfix.org" ;; <soyeomul@doraji.xyz>
        "qmail@list.cr.yp.to" ;; <soyeomul@doraji.xyz>
        "libc-alpha@sourceware.org" ;; <soyeomul@doraji.xyz>
        "gcc@gcc.gnu.org" ;; <soyeomul@doraji.xyz>
	"nullmailer@lists.untroubled.org" ;; <soyeomul@doraji.xyz>
        "getmail@lists.pyropus.ca" ;; <soyeomul@doraji.xyz>
        "pqc-forum@list.nist.gov" ;; <soyeomul@doraji.xyz>
	"gmane-discuss@quimby.gnus.org" ;; /dev/null
	"gmane-test@quimby.gnus.org" ;; /dev/null
       )
)

;;; 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
;;; 마지막 갱신: 2022년 2월 8일

;;; 전자 메일 계정 현황
;; soyeomul@gmail.com: 한우업무 + 개인연락처 -- 그놈에볼루션 + Gnus
;; soyeomul@doraji.xyz: 리눅스/오픈소스 관련 인터넷 계정 -- Gnus
;; soyeomul@gmx.com: 무한대의 저장을 위한 백업용 전자메일 계정 -- Gnus
;; soyeomul@vladivostok.yw.doraji.xyz: 러시아의 구글 얀덱스 -- Gnus
;; soyeomul@kakao.com: 카카오 -- Gnus
;; byunghee.hwang@daum.net: 대한민국의 첫 전자메일 서비스 -- Gnus (UID:1431)
;; 길위의남자: 테스트 테스트 테스트
;; soyeomul@__red_october__: GCP + Amazon-SES -- Gnus
;; soyeomul@__bullseye__: GCP + Amazon-SES -- Gnus
;; soyeomul@__yw__: GCP + 메일건 -- Gnus

;;; 참고문헌:
;; [0] https://en.wikipedia.org/wiki/Gnus
;; [1] http://quimby.gnus.org/
;; [2] https://ashish.blog/2017/09/gnus-multiple-smtp/
;; [3] https://workspaceupdates.googleblog.com/2020/03/less-secure-app-turn-off-suspended.html

;;; ^고맙습니다 _布德天下_ 감사합니다_^))//
