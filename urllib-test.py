#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
from urllib.request import urlopen, Request

# 참고문헌: [0][1]
# [0] https://seunggabi.tistory.com/entry/Python-requests-UserAgent
# [1] https://stackoverflow.com/questions/24226781/changing-user-agent-in-python-3-for-urrlib-request-urlopen

FURL = """
https://httpbin.org/user-agent
"""
FURL = FURL.strip()

MH = """
Python 3 (Ubuntu 18.04)
"""
MH = MH.strip()
MH = {'User-Agent': MH}

def get_text(x, y=None):
    try:
        if y:
            req = Request(x, headers=y)
        else: 
            req = Request(x)
        response = urlopen(req)
        text = response.read().decode("utf-8", "replace")
    except:
        print(sys.exc_info()[0])
        raise

    return text


if __name__ == "__main__":
    print(get_text(FURL))
    time.sleep(3)
    print(get_text(FURL, MH))

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2021년 5월 5일
