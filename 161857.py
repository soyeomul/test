#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import subprocess

# KLDP [161857]

# 텍스트 파일안에 임의의 파일명 목록이 나열되어 있음
# 그 목록을 제외한 나머지 파일들을 삭제해야 함
# find 명령어를 활용할 것
# 시작점 디렉토리는 ./crypto ===> ymir님의 견본으로부터


"""
스크립트 돌리기 직전의 모습입니다:

(bionic)soyeomul@localhost:~/test$ ls -R crypto
crypto:
rand  wrong

crypto/rand:
md_rand.c   rand_err.c  rand_unix.c  rand_win.c
rand_egd.c  rand_lib.c  rand_vms.c   randfile.c

crypto/wrong:
1.lst  2.txt  3.err
(bionic)soyeomul@localhost:~/test$ 
"""

# ymir님께서 제시해주신 견본 파일 목록 (화이트 리스트)
file_list = """\
rand_err.c
rand_win.c
rand_egd.c
rand_unix.c
rand_lib.c
randfile.c
md_rand.c
rand_vms.c
"""

class mc:
    white_list = file_list.split("\n")[:-1]
    
p = subprocess.Popen("find crypto -type f -name '*.*' -print", \
                 stdout=subprocess.PIPE, text=True, shell=True)

output_raw = p.stdout.read()
total_list = output_raw.splitlines(False)

for flist in total_list:
    if flist.split("/")[-1] not in mc.white_list:
        """for 문이기에 하나씩 지웁니다. 수만개의 파일도 지워집니다. 순차적으로"""
        subprocess.call("rm -vf {}".format(flist), shell=True)

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 11일

"""
스크립트 돌린후의 모습:

(bionic)soyeomul@localhost:~/test$ ./161857.py
'crypto/wrong/3.err'가 삭제됨
'crypto/wrong/1.lst'가 삭제됨
'crypto/wrong/2.txt'가 삭제됨
(bionic)soyeomul@localhost:~/test$ ls -R crypto
crypto:
rand  wrong

crypto/rand:
md_rand.c   rand_err.c  rand_unix.c  rand_win.c
rand_egd.c  rand_lib.c  rand_vms.c   randfile.c

crypto/wrong:
(bionic)soyeomul@localhost:~/test$ 
"""


