/* -*- coding: utf-8 -*- */

/* 
 * 문자열 출력: 함수 활용
 *
 * 참고문헌: 
 * 포인터 고급 사용법 <초보자를 위한 리눅스 C 프로그래밍 ISBN 9788980544127>
 */

#include <stdio.h>

void ps(char *p[], int n);

int main(void)
{
	char *s[] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	};

	ps(s, 4);

	return 0;
}

void ps(char *p[], int n)
{
	int i;
	for (i = 0; i < n; i++) {
		printf("%s \n", p[i]);
	}
}

/* 
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 14일
 */
	
	
