# -*- coding: utf-8 -*-

x = "010-7558-5952"
y = x.split("-")
z = "".join(y)

print("x:", type(x))
print("y:", type(y))
print("z:", type(z))

"""
x: <class 'str'>
y: <class 'list'>
z: <class 'str'>
"""
