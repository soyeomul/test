// 고랭지 농법: 문자열과 배열

package main

import "fmt"

func main() {
	계절 := [...]string{
		"봄",
		"여름",
		"가을",
		"겨울",
	}

	fmt.Println("배열길이:", len(계절), "\n")

	for _, value := range 계절 {
		fmt.Println(value)
	}
}

// 이 코드를 통하여
// 고랭은 UTF-8 우대를 하며,
// *컴파일이 가능한 파이썬* 임을 알 수 있어요 ^^^

/*** 코드 실험 결과
soyeomul@penguin:~/go$ ls -l asdf.go
-rw-r--r-- 1 soyeomul soyeomul 451 10월 15 19:06 asdf.go
soyeomul@penguin:~/go$ go run asdf.go
배열길이: 4

봄
여름
가을
겨울
soyeomul@penguin:~/go$ go build asdf.go
soyeomul@penguin:~/go$ ls -l asdf
-rwxr-xr-x 1 soyeomul soyeomul 2101072 10월 15 19:07 asdf
soyeomul@penguin:~/go$ file asdf
asdf: ELF 64-bit LSB executable, ARM aarch64, version 1 (SYSV), statically linked, Go BuildID=DhgOanVTYd6QpGWuGx65/OOe3dpdEgwPvF968scye/jq_d5NnZAKbEuI6kUHxs/Wdz2uWW7CIo3txAwvZku, not stripped
soyeomul@penguin:~/go$ ./asdf
배열길이: 4

봄
여름
가을
겨울
soyeomul@penguin:~/go$ go version
go version go1.15.9 linux/arm64
soyeomul@penguin:~/go$ lsb_release -a
No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux 11 (bullseye)
Release:        11
Codename:       bullseye
soyeomul@penguin:~/go$
***/

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 15일
