#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 순수 한글(UTF-8) 음절만 세기 

import re

p = re.compile("[가-힣]")

문장 = input("입력: ")

갯수 = 0
for 가 in 문장:
    if p.match(가):
        갯수 = 갯수 + 1

print(갯수, repr(문장))

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 2월 24일
