#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests

FURL = """
https://httpbin.org/user-agent
"""
FURL = FURL.strip()

UA = "Python 3 (Ubuntu 18.04)"
MH = {'User-Agent': UA}

def get_text(xyz):
    try:
        req = requests.get(xyz, headers=MH)
        t1 = req.content.decode("utf-8", "replace")
    except:
        raise

    return t1

if __name__ == "__main__":
    print(get_text(FURL))
