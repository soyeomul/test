# -*- coding: utf-8 -*-
# 화면에 3줄의 감사함이 뿌려집니다~

def thanks()
  data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
  return data
end

3.times { print thanks() }

# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 24일
# 마지막 갱신: 2017년 7월 24일
