#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 알파벳 모음 갯수 출력하기

import sys

모음 = [
    "a", "A",
    "e", "E",
    "i", "I",
    "o", "O",
    "u", "U",
]
    
def asdf(ch):
    누적목록 = []

    for k in ch:
        if k in 모음:
            누적목록.append(k)

    return len(누적목록), 누적목록

print(asdf(sys.argv[1]))

# EOF

    
