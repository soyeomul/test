#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# .next로 반복가능한 이터레이터와 제너레이터는 연관성이 깊다

명단 = (
    "김남준",
    "김석진",
    "민윤기",
    "정호석",
    "박지민",
    "김태형",
    "전정국",
)

실험 = iter(명단)

for k in 실험:
    print(k)

print("^ARMY: Top Rank Sustainable Global Group_^))//")

# 편집: Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2020년 12월 25일
