#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# sys.argv 나이 계산 

import sys
from datetime import datetime

cyear = datetime.now().strftime("%Y")

def f(x):
    x = sys.argv[1]
    y = cyear
    return int(y) - int(x) + 1

if len(sys.argv) < 2 or sys.argv[1] == "--help":
    print("사용법: %s <year>" % sys.argv[0])
    sys.exit(1)
else:
    print("당신은 현재 한국나이로 {}살 입니다.".format(f(sys.argv[1])))

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 5월 14일

"""
테스트 결과: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ ./7.py 1977
당신은 현재 한국나이로 43살 입니다.
(bionic)soyeomul@localhost:~/test$ ./7.py --help
사용법: ./7.py <year>
(bionic)soyeomul@localhost:~/test$ ./7.py
사용법: ./7.py <year>
(bionic)soyeomul@localhost:~/test$ 
"""
