# -*- coding: utf-8 -*-

# https://kldp.org/node/161729

f = open("data.txt", "r")
lines = f.readlines()

wf = []
for x in lines:
	wf.append(x.split()[0])

uf = sorted(list(set(wf)))

def make_file(xyz):
	with open("{}.dat".format(xyz), "w") as mf:
		for x in lines:
			if xyz in x:
				mf.write(x)

for x in uf:
	make_file(x)
	print("{}.dat created.".format(x))
        
f.close()

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 14일

"""
data.txt:

435 3190
435 3293
435 2982
132 1023
132 9023
132 3982
132 30223
999 53323
999 232982
"""
