#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

# KLDP [161875]

ifconfig = """\
ens33: flags=4163 mtu 1500
inet 172.16.47.154 netmask 255.255.255.0 broadcast 172.16.47.255
inet6 fe80::20c:29ff:fe39:5d04 prefixlen 64 scopeid 0x20
ether 00:0c:29:39:5d:04 txqueuelen 1000 (Ethernet)
RX packets 44880 bytes 30980088 (29.5 MiB)
RX errors 0 dropped 0 overruns 0 frame 0
TX packets 10768 bytes 4855166 (4.6 MiB)
TX errors 0 dropped 0 overruns 0 carrier 0 collisions 0
"""

line_ip_addr = ifconfig.split("\n")[1]
line_ip6_addr = ifconfig.split("\n")[2]

"""
#IPv4 주소 추출하기
'.' 자체를 걸러내기위한 방편으로 '\'를 바로 앞에다 삽입함
참고문헌: https://kldp.org/comment/631405#comment-631405 (ymir님의 정규표현식)

#IPv6 주소 추출하기: <참고문헌:https://en.wikipedia.org/wiki/IPv6_address>
===> ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff (최대값)
===> ::1 (최소값)
""" 
exp = re.compile("inet ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+) netmask")
exp6 = re.compile("inet6 ([a-fA-F0-9]*:?[a-fA-F0-9]*:?[a-fA-F0-9]*:?[a-fA-F0-9]*:?[a-fA-F0-9]*:?[a-fA-F0-9]*:[a-fA-F0-9]*:[a-fA-F0-9]+) prefixlen")

ip_addr = exp.search(line_ip_addr).group(1)
ip6_addr = exp6.search(line_ip6_addr).group(1)

f = open("t.txt", "r")
lines = f.readlines()
f.close()

with open("t.txt", "w") as wf:
	for line in lines:
		if "1" in line:
			wf.write("{}\n".format(ip_addr))
		elif "2" in line:
			wf.write("{}\n".format(ip6_addr))
		else:
			wf.write(line)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 12일

"""
(bionic)soyeomul@localhost:~/test$ cat t.txt
1
2
3
(bionic)soyeomul@localhost:~/test$ ./161875.py
(bionic)soyeomul@localhost:~/test$ cat t.txt
172.16.47.154
fe80::20c:29ff:fe39:5d04
3
(bionic)soyeomul@localhost:~/test$ 
"""
