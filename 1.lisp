#!/usr/bin/sbcl --script
;; -*- coding: utf-8 -*-

;; 이것은 저의 첫 lisp 코드입니다.

;; SBCL 에서 기본 인코딩을 확인합니다
;; 한글을 위하야^^^
#||
* sb-impl::*default-external-format*

:UTF-8
||#

(write-line "^고맙습니다 _地平天成_ 감사합니다_^))//")

;; (bionic)soyeomul@localhost:~/work$ sbcl --version
;; SBCL 1.4.5.debian

;; 편집: Emacs 26.1 (Ubuntu 18.04)
;; 마지막 갱신: 2019년 1월 31일
