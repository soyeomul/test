/* -*- coding: utf-8 -*- */

/* 상수 예제 */

#include <stdio.h>

int main()
{
	const int a = 3;

	printf("%d\n", a);

	return 0;
}

/*
 * 참고문헌: https://modoocode.com/18
 *
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 17일
 */

