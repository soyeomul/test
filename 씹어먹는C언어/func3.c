#include <stdio.h>
#include <string.h>

char gubun_x[4] = { NULL, };
char gubun_y[5] = { NULL, };
char gubun_z[5] = { NULL, };

void compute(char x[], char y[], char z[])
{
	char xyz[19] = { NULL, };
        char _gubun[2] = { '-', '\0', };
	
	strcat(xyz, x);
	strcat(xyz, _gubun);
	strcat(xyz, y);
	strcat(xyz, _gubun);
	strcat(xyz, z);

	printf("%s \n", xyz); 
}

int main(void)
{
	printf("전화번호 앞 3자리 입력하소: ");
	scanf("%s", &gubun_x);

	printf("중간 4자리 입력하소: ");
	scanf("%s", &gubun_y);

	printf("끝 4자리 입력하소: ");
	scanf("%s", &gubun_z);
	
        compute(&gubun_x, &gubun_y, &gubun_z);
	       
	return 0;
}

/* 함수 공부 세번째: 문자열 합치기 */
