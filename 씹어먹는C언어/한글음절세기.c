/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdbool.h>

bool tf(char *s);
int str_len(char *str);

int main(int argc, char **argv)
{       
 	int i;
	int j;
	int sum = 0;

	char *_str = argv[1];
	
	j = str_len(_str);
	
	for (i = 0; i < j; i++) {
		if (tf(&_str[i]) == 1) {
			sum++;
		}
	}

	printf("전체 문자열 길이는 %d 바이트 입니다\n", j);
	printf("순수 한글 음절: %d 자\n", sum / 3);
	
	return 0;
}

bool tf(char *s)
{
	unsigned mask = 0x80; /* 10000000 (2진수) */

	return (*s & mask);
}

int str_len(char *str)
{
	int len = 0;

	while (str[len]) {
		len++;
	}

	return len;
}

/* 
 * UTF-8 한글 음절 계산: === Public Domain ===
 *
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 15일
 */
