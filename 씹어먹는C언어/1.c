/* -*- coding: utf-8 -*- */

/* 간단한 포인터 예제입니다 */

#include <stdio.h>

int main()
{
	int *gyuri; /* 뱃속 큰딸 규리 */
	int *sua; /* 뱃속 작은딸 수아 */
	int mom; /* 엄마 */

	gyuri = &mom;
	mom = 3; /* 규리를 위하야 딸기 3개 먹음 */
	printf("%p %d\n", gyuri, *gyuri);

	sua = &mom;
	mom = 2; /* 수아를 위하야 딸기 2개 먹었음 */
	printf("%p %d\n", sua, *sua);

	return 0;
}

/*
 * 참고문헌: https://modoocode.com/23
 * 
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 10일
 */
