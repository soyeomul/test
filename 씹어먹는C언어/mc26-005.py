#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 소수의 정의: 1과 자신으로만 나누어지는 정수

# 소수 식별 속도 보완 산법:
# (일반산법) 1과 자신 이외의 수로 나누어 떨어지면 소수가 *아님* ===> "False"
# (보완산법) 입력값을 누적 소수들로 나누어 떨어지면 소수가 *아님* ===> "False" 

# 테스트 유효범위: 2 이상 양의 정수

import sys

lp = 소수_누적값_목록 = []

npt = [] # 합성수 세부 -- 이 값들을 소인수분해에 활용합니다

def g(xyz): # 약수 목록들의 곱 계산
    sum = xyz[1]
    for k in range(2, len(xyz)):
        if len(xyz) >= 3:
            sum = sum * xyz[k]
        else:
            pass
        
    return sum

def d(n): # 합성수 소인수분해
    XYZ = []
    XYZ.append(n)

    if n == 4:
        XYZ.append(2)
        XYZ.append(2)
        npt.append(tuple(XYZ))
        return n, tuple(XYZ[1:])
    
    for k in lp:
        if n % k == 0:
            XYZ.append(k)
    
    if n // g(XYZ) != 1:
        for k in lp:
            for i in npt:
                if n // g(XYZ) == k:
                    XYZ.append(n // g(XYZ))
                elif i[0] == n // g(XYZ):
                    for j in range(1, len(i)):
                        XYZ.append(i[j])

    npt.append(tuple(XYZ))
    
    return n, tuple(XYZ[1:])

def f(x): # 소수 식별 과정
    if x == 2:
        lp.append(x)
        return x, "True"
    
    for n in lp:
        if n != x and x % n == 0:
            print(d(x)) # 합성수 소인수분해
            return x, "False" # 소수 아님을 화면에 출력하고 함수 탈출

    lp.append(x) # 소수로 식별되면 [소수_누적값_목록] 갱신할것
    return x, "True" # 소수를 화면에 출력하고 함수 탈출

범위값 = int(sys.argv[1]) # 2부터 범위값까지 실험
    
for 숫자 in range(2, 범위값+1):
    print(f(숫자))
    
if len(sys.argv) > 2:
    print("소수:", lp, "===>", len(lp))
    print("합성수세부:", npt, "===>", len(npt)) 
    
# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 13일
