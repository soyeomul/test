/* -*- coding: utf-8 -*- */

/*
 * printf -- 출력 양식 예제 
 *
 * https://modoocode.com/7
 */

#include <stdio.h>

int main()
{
	float a = 3.141592f;
	double b = 3.141592;
	int c = 123;

	printf("a: %.3f \n", a);
	printf("c: %5d \n", c);
	printf("b: %5.3f \n", b);

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 3월 6일
 */
