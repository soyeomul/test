/* 
 * 문자열 길이 구하기
 * https://modoocode.com/29
 */

#include <stdio.h>

int str_len(char *str);

int main(void)
{
	char *my_str = "고맙습니다 택배상하차";

	printf("문자열 길이는 %d 입니다.\n", str_len(my_str));

	return 0;
}

int str_len(char *str)
{
	int len = 0;

	while (str[len]) {
		len++;
	}

	return len;
}

/* 
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 5월 31일
 */
