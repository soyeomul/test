/* -*- coding: utf-8 -*- */

/* 
 * <<비트 연산>>
 *
 * 콤푸타로 한글을 표현할 수 있다는것에
 *  연원께 그리고 세종대왕께 깊이깊이 감사드립니다
 * 
 * 참고문헌: [0-2] 
 * [0] https://modoocode.com/8
 * [1] https://www.phpschool.com/link/qna_function/94996  
 * [2] https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/uemacs.git/tree/utf8.c
 */

#include <stdio.h>

int main()
{
	unsigned mask = 0x80; /* 
                               * 0x80 (16진수) = 10000000 (2진수)
                               * ASCII 코드값의 범위는 0 ~ 127 
                               */
	
	unsigned char _str[] = "가나다"; /* UTF-8 한글은 3바이트 */ 
       
 	if (_str[0] & mask) { /* 결과값 1은 참 */
		printf("True: %c%c%c \n", _str[0], _str[1], _str[2]);
	} else { /* 결과값 0은 거짓 */
		printf("False: %c \n", _str[0]);
	}

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 3월 12일
 */
