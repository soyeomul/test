/* -*- coding: utf-8 -*- */

/* 배열과 포인터 */

#include <stdio.h>

int main()
{
	int arr[3] = {13, 18, 31,};
	int *parr0;
	int *parr1;
	int *parr2;

	/* 배열의 값을 포인터 통해서 다른 변수로 넘긴다 */
	parr0 = &arr[0];
	parr1 = &arr[1];
	parr2 = &arr[2];

	printf("%d %p %s %p\n", *parr0, parr0, "--", &arr[0]);
	printf("%d %p %s %p\n", *parr1, parr1, "--", &arr[1]);
	printf("%d %p %s %p\n", *parr2, parr2, "--", &arr[2]);

	printf("%ld \n", sizeof(arr[0])); /* arr[0] 의 크기 */
	printf("%ld \n", sizeof(*parr0)); /* *parr0 의 크기 */
	printf("%ld \n", sizeof(parr0)); /* 포인터의 크기를 반환함 */
	
	return 0;
}
	
/*
 * 참고문헌: https://modoocode.com/25
 * 
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 24일
 */

