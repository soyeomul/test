/* -*- coding: utf-8 -*- */

/*** 한글을 C언어로 처리해봅니다 

(bionic)soyeomul@localhost:~/test$ gcc 한글처리.c
(bionic)soyeomul@localhost:~/test$ ./a.out
0x5e3faee9f0 봄
0x5e3faee9f8 여름
0x5e3faeea00 가을
0x5e3faeea08 겨울
(bionic)soyeomul@localhost:~/test$ 

8바이트씩 차이가 납니다
64비트 시스템의 포인터의 크기는 8바이트인 까닭입니다

***/

#include <stdio.h>

int main()
{
	char *ustr[] = {"봄", "여름", "가을", "겨울",};

	printf("%p %s\n", ustr[0], ustr[0]);
	printf("%p %s\n", ustr[1], ustr[1]);
	printf("%p %s\n", ustr[2], ustr[2]);
	printf("%p %s\n", ustr[3], ustr[3]);

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 3월 5일
 */
