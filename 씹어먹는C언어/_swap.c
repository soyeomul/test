/* -*- coding: utf-8 -*- */

/* 포인터를 통하여 데이타값을 서로 바꾸기 */

#include <stdio.h>

int _swap(int *x, int *y)
{
	/* 빈그릇(tmp)을 활용하여
         * x그릇(&x)과 y그릇(&y)의 물건(데이타값)들을 
         * 서로 바꿔치기하는 기능
         */

	int tmp;

	tmp = *x;

	*x = *y;
	*y = tmp;

	return 0;
}

int main()
{
	int gyuri = 8; /* 규리 그릇에 딸기 8개가 있음 */
	int sua = 3; /* 수아 그릇에 딸기 3개가 있음 */
	printf("%d %d %s %p %p\n", gyuri, sua, "--", &gyuri, &sua);

	_swap(&gyuri, &sua); /* 바꿔치기 수행 */
	printf("%d %d %s %p %p\n", gyuri, sua, "--", &gyuri, &sua);
	
	return 0;
}

/* 
 * 참고문헌:
 * [0] https://modoocode.com/23 
 * [1] 구글(C언어 포인터 스왑)
 *
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 30일
 */
