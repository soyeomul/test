/* -*- coding: utf-8 -*- 
 *
 * 주소값을 활용하여 다른곳으로 매핑함
 * 그리고 그곳 주소값을 찍어보고
 * 그곳의 주소값에 담긴 데이타값도 찍어봄
 */

#include <stdio.h>

int main()
{
	char abc[] = "hello";
	
	char *xyz;
	xyz = &abc; /* 포인터변수는 주소값을 담는 변수입니다 */

	printf("%p %p \n", abc, xyz); 
	printf("%s %s \n", abc, xyz);
	/* 주소값이 같으면 데이타값도 같다는 이치를 보여주는 예제 */

	return 0;
}

/*
 * 편집: VIM (Ubuntu 18.04)
 * 마지막 갱신: 2021년 2월 5일 
 */
