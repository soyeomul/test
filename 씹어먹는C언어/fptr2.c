#include <stdio.h>

int gubun[2][3][2] = {
	{ { 10, 11, }, { 12, 13, }, { 14, 15, } },
	{ { 16, 17, }, { 18, 19, }, { 20, 21, } },
};

void _read(int *arr, int n)
{
	int i;
	for (i=0; i<n; i++) {
		printf("%p %d\n", &arr[i], arr[i]);
	}
}

int main(void)
{	
	_read(&gubun, 12);

	return 0;
}


/* 포인터는 공간의 한계를 뛰어넘는다... 포인터는 위대하다 !!! */
