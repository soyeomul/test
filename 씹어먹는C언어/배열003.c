/* -*- coding: utf-8 -*- */

/* 고차원 배열 예제 */

#include <stdio.h>

int main()
{
	int _num[5][2] = {
		{1, 6},
		{2, 7},
		{3, 8},
		{4, 9},
		{5, 10},
	};

	printf("%d\n", _num[2][0]); /* 3 */
	printf("%d\n", _num[4][1]); /* 10 */

	return 0;
}

/*
 * 참고문헌: https://modoocode.com/20
 *
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 18일
 */
