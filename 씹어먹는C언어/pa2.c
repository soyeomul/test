/* 포인터배열 그리고 함수 */

#include <stdio.h>

void _ps(char *p[], int n);

int main(void)
{
	char *gubun[4] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	};

	_ps(gubun, 4);

	return 0;
}

void _ps(char *p[], int n)
{
	int count;

	for (count=0; count<n; count++) {
		printf("%s \n", p[count]);
	}
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 5월 27일
 */
