/* 문장 입력 받기 재도전 -- 동적 할당 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_QA 3 /* 답변의 최대 갯수 */

int main(void)
{
	char *gubun[MAX_QA] = { NULL, };

	printf("넌 어느별에서 왔니? ");
        gubun[0] = (char *)malloc(99 * sizeof(char));
	gets(gubun[0]);

	printf("너 아침은 먹었냐? ");
        gubun[1] = (char *)malloc(99 * sizeof(char));
	gets(gubun[1]);

	printf("마지막으로 하고싶은말은? ");
        gubun[2] = (char *)malloc(99 * sizeof(char));
	gets(gubun[2]);
	
	puts(gubun[0]);
	puts(gubun[1]);
	puts(gubun[2]);
	
	return 0;
}

/* __끝__ */
