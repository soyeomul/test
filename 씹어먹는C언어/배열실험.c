#include <stdio.h>

void _pp(char *arr, int n);

int main(void)
{
	char gubun_x[4] = { 'a', 'b', 'c', '\0', };
	char gubun_y[4] = "abc";

	_pp(&gubun_x, 4);
	_pp(&gubun_y, 4);
	
	return 0;
}

void _pp(char *arr, int n)
{
	int i;
	for (i = 0; i < n; i++) {
		printf("[%d]", arr[i]);
	}
	
	printf("\n");

	return NULL;
}

/* 배열 실험 */
