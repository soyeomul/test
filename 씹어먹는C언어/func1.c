#include <stdio.h>

char *gubun[] = {
	"봄",
	"여름",
	"가을",
	"겨울",
};

int compute(int n);
int input, output;

int main(void)
{
	printf("맘에 드는 숫자를 입력하시오 [0-65535]: ");
	scanf("%d", &input);

	output = compute(input);
	printf("오! 당신은 %s 을 선택하셨꾼요^^^ \n", gubun[output]);

	return 0;
}

int compute(int n)
{
	int ret;
	
	ret = n % 4;

	return ret;
}

/* 함수 공부 시작~ */
