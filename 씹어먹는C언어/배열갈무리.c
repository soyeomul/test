#include <stdio.h>

int gubun[5] = {
	12, /* 12 마지기 */
	31, /* 31 마지기 */
	10, /* 10 마지기 */
	38, /* 38 마지기 */
	57, /* 57 마지기 */
}; /* 추수 이후의 바닥 볏짚 현황 */

void _rst(int *arr, int n); /* 쟁기질: 논의 흙을 갈아엎는 작업 */

int main(void)
{
	int *ptr[3]; /* 3 군데만 볏짚을 거둘 예정 */

	ptr[0] = &gubun[1]; /* 31 마지기 */
	ptr[1] = &gubun[3]; /* 38 마지기 */
	ptr[2] = &gubun[4]; /* 57 마지기 */

	printf("볏짚 설거지 총량: %d 마지기\n", *ptr[0] + *ptr[1] + *ptr[2]);

	_rst(&gubun, 5); /* 모든 논을 쟁기질 하고서 봄이 올때까지 농한기에 들어감... */

	return 0;
}

void _rst(int *arr, int n)
{
	int i;
	for (i=0; i<n; i++) {
		arr[i] = NULL;
	}
}

/* 배열은 근본이며 농사꾼의 땀과 정성입니다: === Public Domain === */
