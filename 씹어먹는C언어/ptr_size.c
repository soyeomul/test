/* -*- coding: utf-8 -*- */

/* 포인터 크기 */

#include <stdio.h>

int main()
{
	int arr[6] = {8, 2, 3, 4, 5, 6,};
	int *parr = arr;

	printf("%ld \n", sizeof(arr));
	printf("%ld \n", sizeof(parr)); /* 64비트와 32비트 서로 값이 다름 */

	return 0;
}

/*
 * 참고문헌: https://modoocode.com/24
 *
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 19일
 */

