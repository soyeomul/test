/* 구조체 입문: https://modoocode.com/55 */

#include <stdio.h>

struct test {
	char gnus[8];
	int karma;
};

int main(void)
{
	struct test st[3] = {
		{ "2256", -111, },
		{ "8550", -231 },
		{ "2759", -161 },
	};

	printf("%s %d \n", st[2].gnus, st[2].karma);
	
	return 0;
}


/* __EOF__ */
