/* 
 * ### 문자열 자료형 배열 ###
 * C언어 문자열 처리는 열나게 빡세다
 * 그래서 그 개념정리를 위하야 예제를 기록해둔다
 *
 * 문자열 배열은 scanf() 로 받을때 & 를 붙이지 않음
 * 또한 자료구조를 만들땐(write) 배열을 쓰고
 * 그 자료구조를 읽을때엔(read) 포인터를 쓴다
 * 이게 자료의 흐름이며 순리다... 순리대로...
 * 
 * 참고로,
 * 숫자 자료형 배열은 scanf() 로 받을때 & 를 붙인다.
 */

#include <stdio.h>

int main(void)
{
	char gubun[4][7];
	int i;

	for (i=0; i<4; i++) {
	        printf("입력하시오: ");
	        scanf("%s", gubun[i]);
	}

	printf("%s \n", gubun[0]); /* 봄 */
        printf("%s \n", gubun[1]); /* 여름 */
	printf("%s \n", gubun[2]); /* 가을 */
	printf("%s \n", gubun[3]); /* 겨울 */
	printf("%s \n", gubun[4]); /* 쓰레기값 */
	
	return 0;
}

/* 
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 5월 28일
 */ 
