/* 문자열 자료형 포인터배열 */

#include <stdio.h>

int main(void)
{
	char *gubun[] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	};

	int count;

	for (count=0; count<4; count++) {
		printf("%s \n", gubun[count]);
	}

	return 0;
}

/* 
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 5월 27일
 */ 
