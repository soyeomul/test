#include <stdio.h>
#include <limits.h>

#if UINTPTR_MAX == 0xffffffffffffffffULL               
# define BUILD_64   1
#endif

int main(void) 
{
        #ifdef BUILD_64
        printf("Your Build is 64-bit\n");

        #else
        printf("Your Build is 32-bit\n");

        #endif
    
        return 0;
}

/* https://stackoverflow.com/questions/5272825/detecting-64bit-compile-in-c */
/* do not use this: 2021-06-14 */
