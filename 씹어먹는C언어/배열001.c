/* -*- coding: utf-8 -*- */

/* 배열 예제 */

#include <stdio.h>

int main()
{
	int _mem[5] = {0,};
	int i = 0;
	int _sum = 0;

	for (i = 0; i < 5; i++) {
		printf("%d번째 학생의 점수는? ", i + 1);
		scanf("%d", &_mem[i]);
	}

	for (i = 0; i < 5; i++) {
		_sum = _sum + _mem[i];
	}

	printf("전체 학생의 점수 총합은 %d 입니다.\n", _sum);

	return 0;
}

/*
 * 참고문헌: https://modoocode.com/18
 *
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 16일
 */

