/* 포인터배열 다시 테스트... */

#include <stdio.h>

int main(void)
{
	char *gubun[4] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	};

	printf("%s \n", gubun[0]); /* 봄 */
	printf("%s \n", gubun[1]); /* 여름 */
	printf("%s \n", gubun[2]); /* 가을 */
	printf("%s \n", gubun[3]); /* 겨울 */
	printf("%s \n", *gubun); /* 봄  -- [배열 첫번째값의 주소] == [포인터변수의 주소] */
		
	return 0;
}
