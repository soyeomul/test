/* -*- coding: utf-8 -*- */

/* char 자료형과 포인터 */

#include <stdio.h>

int main()
{
	char abc[8] = {'T', 'h', 'a', 'n', 'k', 's', '!', '\0',};

	char *xyz[8];
	xyz[0] = &abc[0];
	xyz[1] = &abc[1];
	xyz[2] = &abc[2];
	xyz[3] = &abc[3];
	xyz[4] = &abc[4];
	xyz[5] = &abc[5];
	xyz[6] = &abc[6];
	xyz[7] = &abc[7];

	printf("%c%c%c%c%c%c%c%c \n", \
	       *xyz[0], *xyz[1], *xyz[2], *xyz[3], \
	       *xyz[4], *xyz[5], *xyz[6], *xyz[7]);
	
	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 2월 5일 
 */
