#include <stdio.h>

int compute(int *x, int *y, char *z);
int input_x, input_y, output;

char choose;

int main(void)
{
	printf("선택하시오 덧셈은 a 뺄셈은 s: ");
	scanf("%c", &choose);
	
	printf("첫번째 수를 입력하시오: ");
	scanf("%d", &input_x);
	printf("두번째 수를 입력하시오: ");
	scanf("%d", &input_y);

	output = compute(&input_x, &input_y, &choose);
	printf("연산 결과: %d\n", output); 

	return 0;
}

int compute(int *x, int *y, char *z)
{
	int ret;
	
	if (*z == 97) {
		ret = *x + *y;
	} else {
		ret = *x - *y;
	}

	return ret;
}

/* 함수 공부 두번째: 도와주세요.. 211.138 칭구에게.. */
