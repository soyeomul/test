/* 문장 입력 받기 재도전 */

#include <stdio.h>
#define MAX_QA 9 /* 답변의 최대 갯수 */
#define MAX_WL 99 /* 답변할 문장의 최대 길이 */

int main(void)
{
	char gubun[MAX_QA][MAX_WL] = { NULL, }; /* 초기화 */

	printf("넌 어느별에서 왔니? ");
	gets(gubun[0]);

	printf("너 아침은 먹었냐? ");
	gets(gubun[1]);

	printf("마지막으로 하고싶은말은? ");
	gets(gubun[2]);
	
	puts(gubun[0]);
	puts(gubun[1]);
	puts(gubun[2]);
	puts(gubun[3]); /* NULL */
	
	return 0;
}

/* __끝__ */
