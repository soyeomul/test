#include <stdio.h>

int main(void)
{
	char str[5] = { 'a', 's', 'd', 'f', '\0', };

	char *ptr;

        ptr = &str[3];

        printf("%c \n", *ptr);

	*ptr = 'z';

	printf("%s \n", str);

	return 0;
}

/* 포인터로 배열에 쓰기도 가능하다 */
