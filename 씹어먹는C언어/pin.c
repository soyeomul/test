/* -*- coding: utf-8 -*- */

/* 포인터 활용으로 엄마-아빠에게 용돈 타기 */

#include <stdio.h>

int mom(int *pin)
{
	return *pin + 8; /* 엄마의 마음 */
}

int daddy(int *pin)
{
	return *pin * 3; /* 아빠의 마음 */
}

int main()
{
	int gyuri;
	gyuri = 100; /* 엄마아빠~ 규리 용돈 100원만 주셔요^^^ */
	
	printf("%s %d\n", "엄마의 규리 용돈:", mom(&gyuri));
	printf("%s %d\n", "아빠의 규리 용돈:", daddy(&gyuri));

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 27일
 */ 
