/* 구조체 입문: https://modoocode.com/55 */

#include <stdio.h>

struct test {
	char gnus[8];
	int karma;
};

int main(void)
{
	struct test st[3] = {
		{ "2256", -111, },
		{ "8550", -231, },
		{ "2759", -161, },
	};

        struct test *ptr;
	
	ptr = &st[2]; /* 불스아이의 배열을 포인터로 연결함 */
	ptr->karma = NULL; /* 업보 총량 초기화시킴 */
	
        printf("%d \n", st[2].karma); /* 초기화 확인: NULL == 0 */ 
	printf("%s \n", ptr->gnus); /* 불스아이 호출 */

	printf("옥황상제: 너는 이제 불스아이(Bullsleye) 로 다시 태어났다.\n");
	printf("옥황상제: 지상으로 내려가서 온누리에 리눅스를 널리널리 보급하는데 이바지하라.\n");
	printf("불스아이(%s): \t 예, 명을 받들어 시행하겠나이다!\n", ptr->gnus);

	/* 그리고 신축년(2021년) 어느날 데비안 11 안정판 Bullseye 가 출시되었다. */
	
	return 0;
}


/* __EOF__ */
