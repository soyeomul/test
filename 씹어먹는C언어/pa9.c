#include <stdio.h>

char gnus[3][8] = { "2256", "8550", "2759", }; /* 명부전 대기중인 소들 명단 */
int karma[3] = { -111, -231, -161, }; /* 명부전 대기중인 소들의 각 업보 총량 */

void _call(char *k[], int n);
void _reset(int n);

int main(void)
{
	_reset(2); /* 불스아이 업보 초기화 시킴 */
	_call(gnus, 2); /* 불스아이 호출함 */
       
	puts("옥황상제: 너의 이름은 이제 불스아이(Bullseye) 다.");
	puts("옥황상제: 지상으로 내려가 리눅스를 세상에 널리널리 보급하라.");
	puts("불스아이(2759): 예, 명을 받들어 시행하겠습니다!");

	/* 그리고 신축년(2021년) 어느날 데비안 불스아이가 출시된다. */
	
	return 0;
}

void _call(char *k[], int n)
{
        printf("%s \n", &k[n]);
}

void _reset(int n)
{
	int *ptr = karma;
	ptr[n] = 0;

	printf("%d \n", karma[n]);
}

/* __EOF__ */
