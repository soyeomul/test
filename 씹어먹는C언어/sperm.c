/* -*- coding: utf-8 -*- */

/* 포인터로 배열에 연결하여 데이타값 받아와서 연산하기 */

#include <stdio.h>

int _compute(int *sperm)
{
	int mom = 3; /* 엄마의 식별자 */
	const int gyuri = 11; /* 규리(큰딸)의 식별자 */
	
	if (*sperm + mom == gyuri) {
		printf("%s %d %s %p\n", "아빠의 식별자:", *sperm, "--", sperm);
	}

}
      		  
int main()
{
	int daddy[5] = {11, 10, 9, 8, 7,}; /* 아빠의 데이타베이스 */

	int i;
	for (i = 0; i < 5; i++) {
		_compute(&daddy[i]); /* 하늘과 땅이 어우러짐 */
	}

	return 0;
}
		     
/*
 * 편집: GNU Emacs 27.1 (Ubuntu 18.04)
 * 마지막 갱신: 2021년 1월 30일
 */
