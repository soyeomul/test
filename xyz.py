#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import threading

#
# 불교의 교훈: 미륵불(=상제님) 이야기
#

# === Public Domain ===

업보 = -16 # 선천 오만년동안 서로간에 맺힌 원한의 상극적 에너지

def 삶(xyz):
    노력 = xyz
    if 업보:
        복록 = 노력 + 업보
        return "복록", str(복록)

    return "복록", str(노력 * 3000)


def 업보의굴레():
    while True:
        for 업보의바퀴 in "-\\|/":
            sys.stdout.write(업보의바퀴)
            sys.stdout.flush()
            time.sleep(0.3)
            sys.stdout.write("\b")
            if 해원:
                sys.stdout.write("업보를 벗고 업보의 수레바퀴로부터 탈출합니다\n")
                return
            
            
if __name__ == "__main__":
    sys.stdout.write("%s%s" % (
        삶(xyz=3),
        "\n",
    ))

    해원 = False
    
    업보의수레바퀴 = threading.Thread(target=업보의굴레)
    업보의수레바퀴.start()

    time.sleep(13) # 미륵불을 만날때까지 업보의 굴레에서 헤매입니다

    해원 = True # 미륵불을 만나서 업보를 벗습니다

    업보의수레바퀴.join() # ... 업보(karma)의 지도 재작성중 ...
    time.sleep(3)      # ... 업보(karma)의 지도 재작성중 ...

    업보 = 0
    업보 = None # 업보 데이타베이스 초기화(RESET) ===> 새삶이 시작됨

    """ 
               RESET 의 사전적 의미:
                오류를 모두 없애고 시스템을 정상상태 또는 초기상태(0의 상태)
                로 되돌리는 일.
    """

    sys.stdout.write("%s" % (
        삶(xyz=3),
    )) # 업보를 벗긴 이후의 복록 재할당
    sys.stdout.write("\n^고맙습니다 감사합니다_^))//\n")

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 5월 12일
