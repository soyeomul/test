#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import binascii
import subprocess

FURL = "https://gitlab.com/soyeomul/stuff/raw/master/%ED%95%9C%EC%9D%BC%EC%A0%84/%ED%95%9C%EC%9D%BC%EC%A0%84.hexdata"
subprocess.call("rm -f /tmp/*.hexdata* /tmp/data.*; wget -q -P /tmp " + FURL, shell=True)

f = open("/tmp/한일전.hexdata", "r"); data = f.read()

data = data.strip()
data = data.replace(' ', '')
data = data.replace('\n', '')
data = data[2:-1]

data = binascii.a2b_hex(data)
with open("/tmp/data.png", "wb") as df:
    df.write(data)

f.close()
df.close()

subprocess.call("firefox /tmp/data.png", shell=True)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 4월 28일
