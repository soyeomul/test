#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 출처:
# https://wiki.debian.org/DebianWomen/PythonTutorial

# 파일의 줄 수 세기

import sys

def _lc(xyz):
    f = open(xyz, 'r')
    line_count = 0
    while True:
        line = f.readline()
        if not line:
            break
        line_count = line_count + 1

    f.close()
    
    return line_count


if __name__ == "__main__":
    FPATH = sys.argv[1]
    sys.stdout.write("%d lines\n" % _lc(FPATH))
    
# __EOF__
