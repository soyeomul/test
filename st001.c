/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

struct _s {
	char *comment;
};

int main(void)
{
	struct _s s[3] = { NULL, };
	
	s[0].comment = malloc(100); /* 공백포함 ascii 99자를 쓸 수 있음 */
	scanf("%s", s[0].comment);

	s[2].comment = malloc(50); /* 공백포함 ascii 49자를 쓸 수 있음 */
	scanf("%s", s[2].comment);

	printf("%s\n%s\n%s\n", s[0].comment, s[1].comment, s[2].comment);

	
	return 0;
}
 
/* 
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 24일
 */
	
