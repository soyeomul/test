#!/usr/bin/ruby1.8
# -*- coding: utf-8 -*-

FPATH = "/home/soyeomul/hanwoo/001sjh.txt"
f = IO.read(FPATH)

exp_all = Regexp.new("^[0-9]{4}.*NA\\)$", Regexp::MULTILINE)

exp_data = exp_all.match(f)
split_exp_data = exp_data.to_s.split("\n")

split_exp_data.delete_if() do |item|
  item.length != 57
end
# 배열내에서 리스트의 크기가 57 이 아닌것을 모두다 제거함

puts split_exp_data
puts split_exp_data.length

=begin
루비 정규표현식이 파이썬처럼 정확하게 걸러주지 않아서
 배열내에서 리스트의 조건문에 부합하지 않는걸 추려내는 코드 예제.
 
출처는
 생활코딩 -> 언어 -> Python & Ruby -> 루비의 코드블록;;;
=end
