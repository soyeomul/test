/* -*- coding: utf-8 -*- */

#include <stdio.h>

int _odd(int xyz)
{
	return (xyz % 2) == 1;
}

int main(void)
{
	int idx;
	int (*ptr_odd)(int xyz);

	ptr_odd = _odd;

	for (idx=1; idx<11; idx++) {
		if (ptr_odd(idx) == 1) {
			printf("%d \n", idx);
		}
	}

    return 0;
}

/* EOF */
