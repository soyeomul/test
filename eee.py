#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

# 주어진 텍스트에서 전자메일 주소만 추출하기
# 완벽하지 않습니다 *** 절대로 실전에선 쓰지 마세요 ***

# 참고문헌: https://irclogs.ubuntu.com/2019/07/18/%23ubuntu-ko.html

data_raw = """\
John Smith jsmith@xcf.berkeley.edu
홍길동 070-333-8888
staff@navy.mil 익명의 미합중국 해군 중사
010-7558-5952 익명의 전화번호
010_7558_5952@daum.net 전화번호 양식의 한메일 계정 
농협 302-13-545-874585 bbccdd.1214@gmail.com
soyeomul.ubuntu+study@kr.asdf-qwer.xyz 익명의 구글앱스
"""

data = data_raw.split("\n")

def chop(xyz): # 혼미한 정규표현식... 아아아... 작성자도 이해가 안감... 결과는 나옴... ㅠㅠㅠ
    exp = re.compile("(([a-zA-Z0-9][a-zA-Z0-9]*[.]?[_]?[a-zA-Z0-9+]*)+@([a-zA-Z0-9][a-zA-Z0-9-]*[.][a-zA-Z0-9-.]*)+[a-zA-Z]+)")
    str = exp.search(xyz).group(1)

    return str

for v in data:
    if "@" in v:
        print(chop(v))

"""
(bionic)soyeomul@localhost:~/test$ ./eee.py
jsmith@xcf.berkeley.edu
staff@navy.mil
010_7558_5952@daum.net
bbccdd.1214@gmail.com
soyeomul.ubuntu+study@kr.asdf-qwer.xyz
"""

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 18일
