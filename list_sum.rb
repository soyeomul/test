#!/usr/bin/ruby1.8
# -*- coding: utf-8 -*-

# 각 리스트의 합을 구하는 코드입니다.

x = 2**1000
y = x.to_s.split("")
z = y.map(&:to_i).inject(0, :+)

puts z # 결과값: 1366
