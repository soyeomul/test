#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 파일명: _out.py

# KLDP [163316]

# JSON 파일로부터 seq, port 정보 출력하기

import json
import sys

FPATH = sys.argv[1]

f = open(FPATH, "r")
data = f.read(); f.close()

dj = json.loads(data)

def _out(xyz):
    seq = dj['{0}'.format(xyz)]['seq']
    port = dj['{0}'.format(xyz)]['port']
    
    return xyz, seq, port

for x in dj.keys():
    print(_out(x))

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 5월 19일
