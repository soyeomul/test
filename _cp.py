#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 파일명: _cp.py

import subprocess
import sys

# 현재 디렉토리에 존재하는 모든 그림 파일들을 찾아서
#  TARGET_DIR 으로 복사하는 기능
#   - 하위 디렉토리까지 검색함
#   - 확장자 대소문자 구분안함

"""
### 초간단 도움말 ###
sys.argv[1] 인수를 받아서 원하는 확장자를 가진 파일들을 다 찾아냅니다
그리고 TARGET_DIR 으로 복사합니다
예문: $ ./_cp.py png
     $ ./_cp.py jpg
     $ ./_cp.py svg
"""

### 반드시 현재 디렉토리에서 실험하시길 권유드립니다 ###

TARGET_DIR = "~/bak" # 변경 가능

subprocess.call("mkdir -p {0}".format(TARGET_DIR), shell=True)

def _cp(x):
    cmd_cp = "cp -vf '{0}' {1}".format(x, TARGET_DIR)
    subprocess.call(cmd_cp, shell=True)

    return

cmd_ls = "find . -iname '*.{0}' -print".format(sys.argv[1])

p = subprocess.Popen(cmd_ls, stdout=subprocess.PIPE, shell=True)
data = p.communicate()[0].decode("utf-8")

data_lst = data.strip().split("\n")

for k in data_lst:
    _cp(k)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 5월 23일
