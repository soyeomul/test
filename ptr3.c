/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

/*
 * === 함수 포인터 연습 ===
 *
 * 참고문헌:
 * <https://reakwon.tistory.com/17>
 */ 

int p(int x, int y)
{
	return x + y;
}

int m(int x, int y)
{
	return x - y;
}

int main(void)
{
	int x = 8;
	int y = 3;

	int (*ptr)(int x, int y);
       
	char op;
	
	puts("plus is 'p' and minus is 'm'.");
	puts("please choice 'p' or 'm': ");
	scanf("%c", &op); 

	switch (op) {
	case 'P':
	case 'p':
		ptr = p;
		printf("%d \n", ptr(x, y));
		break;
	case 'M':
	case 'm':
		ptr = m;
		printf("%d \n", ptr(x, y));
		break;
	default:
		printf("you wrong! \n");
		exit(1);
	}

	printf("thanks! \n");

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 10월 21일
 */
