# -*- coding: utf-8 -*-

# 십진법을 이진법으로 변환
# === Public Domain ===

import sys

def _dm(x, y):
    q = x // y
    r = x % y

    return q, r

    
_input = int(sys.argv[1])
total_numbers = []

몫 = _input

while True:
    몫, 나머지 = _dm(몫, 2)
    total_numbers.append(나머지)
    if 몫 == 0:
        break

print(total_numbers[::-1])

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 5월 23일
