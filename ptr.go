// 포인터 예제

package main

import "fmt"

func hello(n *int) {
	*n = 2
}

func main() {
	var n int = 1

	fmt.Println(n)

	hello(&n)
	// 주소로 접근한다
	// 같은 주소에 담긴 값을 설거지함...
	// 끝내줌!!!
	// 저걸 포인터로 접근 안하고 같은 시도를 하면
	// n 값 안바뀜. 두 곳의 주소가 다르기에...

	fmt.Println(n)
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 18일
