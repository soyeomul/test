/* -*- coding: utf-8 -*- */

#include <stdio.h>

int hello(int *n)
{
	*n = 2;
}
	
int main(void)
{
	int n = 1;
	printf("%d \n", n);
	
	hello(&n);
	printf("%d \n", n);

	return 0;
}

/* 
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 10월 19일
 */

  
