/* -*- coding: utf-8 -*/

/* 참고문헌: https://www.joinc.co.kr/w/man/2/getuid */

#include <stdio.h>
#include <unistd.h>

int main()
{
	printf("%d \n", getuid());

	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2022년 2월 19일 
 */

/*** ssmtp 재컴파일 순서

1. git clone https://salsa.debian.org/debian/ssmtp.git
2. ssmtp.c 패치
3. sudo apt update; upgrade 로 중간설거지
4. fake quilt 두 패키지 설치
5. 의존성 설거지: sudo apt build-dep ssmtp
6. 재컴파일 시도: sudo dpkg-buildpackage -A
7. ssmtp 바이너리 퍼미션 점검: owner root / group mail / g+s

# 참고문헌: [0-3]
[0] https://wiki.debian.org/UsingQuilt
[1] https://wiki.debian.org/BuildingTutorial
[2] https://unix.stackexchange.com/questions/220460/how-to-build-a-specific-package-from-a-debian-source-package
[3] https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1005970

***/
