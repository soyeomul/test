# -*- coding: utf-8 -*-

# 파이썬3 표준 함수 ctypes() 실험

import ctypes
import sys

SLPATH = "./mcall.so"
libc = ctypes.CDLL(SLPATH)


if __name__ == "__main__":
    input = int(sys.argv[1])
    v = libc.mcall(input)
    print(v)

"""
# 참고문헌:
[1] https://docs.python.org/ko/3/library/ctypes.html
[2] https://goodtogreate.tistory.com/entry/Ctypes%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%9C-Python%EA%B3%BC-C%EC%99%80%EC%9D%98-%EC%97%B0%EA%B2%B0

# 사용된 C언어 코드
/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

int mcall(int n)
{
        return n * 3;
}

int main(int argc, char **argv)
{
        int i = atoi(argv[1]);
        printf("%d \n", mcall(i));

        return 0;
}

# C언어 코드 공유 라이브러리 생성 방법
$ gcc -shared -o mcall.so mcall.c
"""

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 9월 28일
