#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys

# 10진수를 2진수로 변환 (비트연산 활용)

nnnn = int(sys.argv[1]) # 유효값 [1-255]
mask = 0b10000000

bin_str = []
for i in range(0, 8):
	if (mask & nnnn) == mask:
		bin_str.append("1")	
	else:
		bin_str.append("0")
	mask = mask >> 1

print("{}{}, {}, {}".format("".join(bin_str), "(2)", bin(nnnn), nnnn))

# 참고문헌: https://wikidocs.net/20704

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 29일
