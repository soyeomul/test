# -*- coding: utf-8 -*-

import sys

if sys.hexversion >= 0x020716F0:
    print("Yes!!!")
else:
    print("Sorry...")

print("sys.hexversion:", hex(sys.hexversion))

# https://docs.python.org/3/library/sys.html#sys.hexversion

