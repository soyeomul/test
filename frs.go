// 고랭지 농법: 문자열 뒤집기
//
// 참고문헌:
// <https://golangbyexample.com/reverse-a-string-in-golang/>

package main

import "fmt"
import "strings" /* Join() */
import _ "reflect"

func main() {
	s := "abc-1130-xyz"

	fmt.Println(frs(s))
}

func frs(s string) string {
	idx := len(s)

	ss := make([]string, 0, idx)
	for i := idx - 1; i >= 0; i-- {
		ss = append(ss, string(s[i]))
	}

	rs := strings.Join(ss[:], "")

	return rs
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 16일
