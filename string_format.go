// 고랭지 농법: 문자열 양식 꾸미기
//
// 참고문헌:
// <https://zetcode.com/golang/string-format/>

package main

import "fmt"

func main() {
	uid := "soyeomul"
	mark := "@"
	fqdn := "gopher.yw.doraji.xyz"

	res := fmt.Sprintf("%s%s%s",
		uid,
		mark,
		fqdn,
	)

	fmt.Println(res)
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 16일
