#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re

# https://www.ipaddressguide.com/ipv6-to-decimal

def f(xyz): # 축약형 주소를 확장형 주소로 변경하는 함수
    cs = xyz.count(":") # 콜론 갯수
    ms = 7 - cs + 1 # 확장형으로 변경시 추가로 삽입하는 콜론 갯수
    list_xyz = list(xyz) # 확장형으로 변경시 리스트에서 작업함
    ss = re.search("::", xyz) # 축약형 탐지 사전작업
    if xyz == "::1":
        return ":::::::1"
    elif "::" in xyz and xyz != "::1":
        sp = ss.start() # 콜론 삽입 위치
        list_xyz[sp] = ":" * ms # 축약형을 확장형으로 전환
        return "".join(list_xyz) # 전환된것을 반환
    else:
        return xyz
        
ip6_addr = f(sys.argv[1]) # 확장형 주소

exp = re.compile("^(.*):(.*):(.*):(.*):(.*):(.*):(.*):(.*)$")

# str.zfill 함수를 적용하여 빈자리를 0 으로 채워서 자리수를 맞춰줌
chop1 = exp.search(ip6_addr).group(1)
chop1 = chop1.zfill(4)

chop2 = exp.search(ip6_addr).group(2)
chop2 = chop2.zfill(4)

chop3 = exp.search(ip6_addr).group(3)
chop3 = chop3.zfill(4)

chop4 = exp.search(ip6_addr).group(4)
chop4 = chop4.zfill(4)

chop5 = exp.search(ip6_addr).group(5)
chop5 = chop5.zfill(4)

chop6 = exp.search(ip6_addr).group(6)
chop6 = chop6.zfill(4)

chop7 = exp.search(ip6_addr).group(7)
chop7 = chop7.zfill(4)

chop8 = exp.search(ip6_addr).group(8)
chop8 = chop8.zfill(4)

rs = chop1 + chop2 + chop3 + chop4 + chop5 + chop6 + chop7 + chop8

print(ip6_addr) # 디버깅 위하야...

hex_rs = "0x" + rs
print(hex_rs, len(hex_rs) - 2) # 디버깅 위하야... 

int_rs = int(hex_rs, 16)
print(int_rs) # 최종 결과값 (십진수)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 21일
