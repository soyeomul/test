# -*- coding: utf-8 -*-

# KLDP [162547]

import re

# swish95님의 견해를 충족하기위하야,,,
# 메모리를 아끼려 한줄씩 읽어서 처리하되
# 따옴표(")나 세미콜론(;)중 하나가 부족하면
# 추출이 불가능하므로 해당문자열을 그대로 보여주도록 설정했습니다. 
#
# 또한,
# (1) 추출실패는 "X", 추출성공은 "O" 표기를 각각 출력값 머릿말에 붙입니다.
# (2) 성공/실패 모두다 해당줄번호도 함께 출력합니다. 
# 두항목 모두 결과값 식별을 쉽게하기 위함입니다.
#
# 테스트를 위하야 마지막 한줄 더 추가했습니다 ===> EOF
#
# [파이썬 3.7, 우분투 18.04 에서 테스트를 했습니다]

"""
$ cat TEST_FILE
ABC"DEF;G
AB
C"DE
F;G
EOF
"""

s1 = "\""
s2 = ";"

def lsearch(line):
    m1 = re.search(s1, line)
    m2 = re.search(s2, line)

    i1 = m1.start() + 1
    i2 = m2.start()

    return line[i1:i2]

lf = open("TEST_FILE", "r")
lm = 1
ldata = lf.readline()

while ldata:
    if s1 in ldata and s2 in ldata:
        print("O", str(lm).zfill(3), lsearch(ldata))
    else:
        print("X", str(lm).zfill(3), repr(ldata))
        
    ldata = lf.readline()
    lm = lm + 1
    
lf.close()

# 참고문헌: *한줄씩 읽어들이는 readline() 예제 코드*
# https://m.blog.naver.com/javaking75/221134977155

# 편집: Emacs 26.3 (Ubuntu 18.04)
# 마지막갱신: 2019-12-19

"""
아래는 분석 불가능한 줄만 다음줄과 병합하여
처리하는 예제 코드를 부록으로 기록합니다.
"""

"""
# -*- coding: utf-8 -*-

import re

s1 = "\""
s2 = ";"

def lsearch(line):
    m1 = re.search(s1, line)
    m2 = re.search(s2, line)

    i1 = m1.start() + 1
    i2 = m2.start()

    return line[i1:i2]

lf = open("TEST_FILE", "r")
lm = 1
ldata = lf.readlines()

for idx in range(len(ldata)):
    if s1 in ldata[idx] and s2 not in ldata[idx]:
        ldata[idx] = (ldata[idx]+ldata[idx+1]).replace("\n", "")
        ldata[idx+1] = "\n"
    else:
        pass

for idx in range(len(ldata)):
    if s1 in ldata[idx] and s2 in ldata[idx]:
        print("O", str(idx+1).zfill(3), lsearch(ldata[idx]))
    else:
        print("X", str(idx+1).zfill(3), repr(ldata[idx]))

lf.close()
"""
