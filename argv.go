// argv test
//
// 참고문헌:
// <https://stackoverflow.com/questions/3356011/whats-gos-equivalent-of-argv0>
// <https://johngrib.github.io/wiki/golang-reflect/>

package main

import "os"
import "fmt"
import "reflect"

func main() {
	s0 := os.Args[0]
	s1 := os.Args[1]

	fmt.Println(s0)
	fmt.Println(reflect.ValueOf(s0).Type())

	fmt.Println()

	fmt.Println(s1)
	fmt.Println(reflect.ValueOf(s1).Type())
}

// 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
// 마지막 갱신: 2021년 10월 16일
