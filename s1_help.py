#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

# 한달치 검색 쉘스크립트 자동으로 만들어줍니다.
# "s1.sh" 라는 실행파일이 생성되구요,,,
# ... 파이썬 3.7 필요합니다 ...

# 참고문헌: [s1.py] 
# https://gitlab.com/soyeomul/test/raw/master/s1.py

import subprocess
import sys

if len(sys.argv) < 3:
	print("사용법: {} 찾을문자열 찾을년월".format(sys.argv[0]))
	print("$ {} 안녕하세요 201501".format(sys.argv[0]))
	sys.exit(1)

top_string = "#!/bin/sh\n"

# sys.argv[1] ===> 찾을문자열
# sys.argv[2] ===> 찾을 년도와 월: 201901
ss = "./s1.py {0} {1}".format(sys.argv[1], sys.argv[2])

ds = list(range(1, 32))

date = []
for x in ds:
	date.append(str(x).zfill(2))

rs = []
for i in date:
	rs.append(ss + i)

rs_data = "\n".join(rs)

with open("s1.sh", "w") as f:
	f.write(top_string)
	f.write(rs_data)

subprocess.call("wget -q https://gitlab.com/soyeomul/test/raw/master/s1.py", shell=True)
subprocess.call("chmod +x s1.py", shell=True)
subprocess.call("chmod +x s1.sh", shell=True)
print("s1.sh 실행파일이 생성되었습니다.")
subprocess.call("ls -l s1.sh", shell=True)

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 21일
