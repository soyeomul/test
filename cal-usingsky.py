#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 양력 <-> 음력 양자간 날짜를 확인할 수 있는 파이썬 코드.
# 한국천문연구원의 자료를 바탕으로 만들어진 코드.
# 음력변환은 1391년 1월 1일부터 2050년 11월 18일까지만 확인 가능하다고 함.
#
# 만든분: usingsky@gmail.com
# 코드 주소: https://github.com/usingsky/korean_lunar_calendar_py

# 사용법: pip3 로 해당 파이썬 패키지를 설치합니다.
# pip3 install korean_lunar_calendar

# 그리고 다음처럼 짭니다.
# 예시는 음력 2019년 9월 19일을 양력으로 변환하여 출력해줍니다.

from korean_lunar_calendar import KoreanLunarCalendar

calendar = KoreanLunarCalendar()
calendar.setLunarDate(2019, 9, 19, False)

print(calendar.SolarIsoFormat())

# 출력 결과와 실제 달력상의 날짜를 확인해보니 정확히 맞아떨어집니다.
# ^감사합니다_^))//

# 본 코드는 우분투 리눅스 18.04, 파이썬 3.6.7 에서 실행되었습니다.

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 1월 27일
