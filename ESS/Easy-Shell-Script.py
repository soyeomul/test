# -*- coding: utf-8 -*-

#
# <<처음 배우는 셸 스크립트>>
#

import json

_1부 = "1부 %s" % ("셸 스크립트 기초")
_2부 = "2부 %s" % ("가장 많이 쓰이는 리눅스 명령어")
_3부 = "3부 %s" % ("예제와 함께 하는 셸 스크립트 활용")

_1부_목록 = [
    "CHAPTER 1 %s" % ("셸 스크립트란?"),
    "CHAPTER 2 %s" % ("셸 스크립트 기초 문법"),
]

_2부_목록 = [
    "CHAPTER 3 %s" % ("문자열을 찾을 수 있는 grep"),
    "CHAPTER 4 %s" % ("파일을 찾을 수 있는 find"),
    "CHAPTER 5 %s" % ("특정 인덱스 문자열을 출력할 수 있는 awk"),
    "CHAPTER 6 %s" % ("찾은 문자열을 바꿀 수 있는 sed"),
    "CHAPTER 7 %s" % ("날짜와 시간을 알려주는 date"),
]    

_3부_목록 = [
    "CHAPTER 8 %s" % ("시스템 구축"),
    "CHAPTER 9 %s" % ("환경 설정"),
    "CHAPTER 10 %s" % ("보안"),
    "CHAPTER 11 %s" % ("모니터링"),
    "CHAPTER 12 %s" % ("클라우드 시스템 운영"),
    "CHAPTER 13 %s" % ("퍼블릭 클라우드 사용"),
]    

### CREDIT BEGINS HERE
AUTHOR = "저자: %s, %s" % ("장현정", "레드햇 한국")
ISBN = "ISBN: %s" % ("979-11-6224-389-3")
TITLE = "제목: %s" % ("『처음 배우는 셸 스크립트』")
PUBLISHER = "출판사: %s" % ("한빛미디어")
PUBLISHED = "초판 발행: %s" % ("2021년 2월 1일")

CREDIT = [AUTHOR, TITLE, ISBN, PUBLISHER, PUBLISHED,]
CREDIT = dict([tuple(x) for x in enumerate(CREDIT)])
### CREDIT ENDS HERE

###
### SECTION(절;節) 구문 분석 시작
###

def _get_section(xyz):
    from subprocess import Popen, PIPE
    """
    `curl' 은 Linux/*BSD 등에서 유명한 도구입니다
    본 코드는 그래서 가급적 Linux/*BSD 시스템에서 실험하시길 권유드립니다
    """
    _cmd = "curl -s -f {0} | grep -v '^#' | grep -v '^$'".format(xyz)
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8").strip()

    return output

FURL = "https://gitlab.com/soyeomul/test/-/raw/master/ESS/Sections-ESS.lst"

_절_뭉치 = _get_section(FURL)
_절_목록 = _절_뭉치.splitlines(False)

def _make_part(xyz):

    r = _range = [5, 6, 2, 3, 3, 3, 3, 6, 5, 5, 8, 8, 4,]
   
    s = _offset = [
        r[0],
        r[0]+r[1],
        r[0]+r[1]+r[2],
        r[0]+r[1]+r[2]+r[3],
        r[0]+r[1]+r[2]+r[3]+r[4],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7]+r[8],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7]+r[8]+r[9],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7]+r[8]+r[9]+r[10],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7]+r[8]+r[9]+r[10]+r[11],
        r[0]+r[1]+r[2]+r[3]+r[4]+r[5]+r[6]+r[7]+r[8]+r[9]+r[10]+r[11]+r[12],
    ]

    새목록 = []
    
    if "_1부_목록" in xyz:
        새목록.append(_절_목록[:s[0]])
        새목록.append(_절_목록[s[0]:s[1]])
    if "_2부_목록" in xyz:
        새목록.append(_절_목록[s[1]:s[2]])
        새목록.append(_절_목록[s[2]:s[3]])
        새목록.append(_절_목록[s[3]:s[4]])
        새목록.append(_절_목록[s[4]:s[5]])
        새목록.append(_절_목록[s[5]:s[6]])
    if "_3부_목록" in xyz:
        새목록.append(_절_목록[s[6]:s[7]])
        새목록.append(_절_목록[s[7]:s[8]])
        새목록.append(_절_목록[s[8]:s[9]])
        새목록.append(_절_목록[s[9]:s[10]])
        새목록.append(_절_목록[s[10]:s[11]])
        새목록.append(_절_목록[s[11]:s[12]])

    return 새목록

###
### SECTION(절;節) 구문 분석 끝
###

_1부_새목록 = _make_part("_1부_목록")
_1부_새목록 = dict(zip(_1부_목록, _1부_새목록))

_2부_새목록 = _make_part("_2부_목록")
_2부_새목록 = dict(zip(_2부_목록, _2부_새목록))

_3부_새목록 = _make_part("_3부_목록")
_3부_새목록 = dict(zip(_3부_목록, _3부_새목록))

KEY = [_1부, _2부, _3부, "CREDIT",]
VALUE = \
    [_1부_새목록] + \
    [_2부_새목록] + \
    [_3부_새목록] + \
    [CREDIT]

dd = dict(zip(KEY, VALUE))

dj = json.dumps(dd, indent=8, ensure_ascii=False)

if __name__ == "__main__":
    print(dj)

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 3월 8일
