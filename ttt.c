/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char *a;
	a = "asdf";
	/* 문자열 초기화는 자동으로 메모리가 할당됨 */

	printf("%s \n", a);

	char *b;
	b = malloc(99);
        /* scanf() 는 말록이 필요하다 */
	
	scanf("%s", b);

	printf("%s \n", b);
	
	return 0;
}

/*
 * 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
 * 마지막 갱신: 2021년 11월 24일
 */
