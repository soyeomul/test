/* -*- coding: utf-8 -*- */

/***
 ### malloc 으로 만들어진 배열의 실제 크기 구하기 ###
 # 실험환경: 우분투 18.04 LTS (ARM64 크롬북 -- 코드명: birch)
 
 (bionic)soyeomul@localhost:~/111$ gcc 3.c
 (bionic)soyeomul@localhost:~/111$ ls
 3.c  a.out
 (bionic)soyeomul@localhost:~/111$ ./a.out 0
 sorry
 (bionic)soyeomul@localhost:~/111$ ./a.out 1
 0 
 (24 / 4 = 6) # 실제 할당된 배열 크기
 1 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 3
 0 1 2 
 (24 / 4 = 6) # 실제 할당된 배열 크기
 3 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 6
 0 1 2 3 4 5 
 (24 / 4 = 6) # 실제 할당된 배열 크기
 6 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 8
 0 1 2 3 4 5 6 7 
 (40 / 4 = 10) # 실제 할당된 배열 크기
 8 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 10
 0 1 2 3 4 5 6 7 8 9 
 (40 / 4 = 10) # 실제 할당된 배열 크기
 10 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 13
 0 1 2 3 4 5 6 7 8 9 10 11 12 
 (56 / 4 = 14) # 실제 할당된 배열 크기
 13 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 14
 0 1 2 3 4 5 6 7 8 9 10 11 12 13 
 (56 / 4 = 14) # 실제 할당된 배열 크기
 14 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 18
 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 
 (72 / 4 = 18) # 실제 할당된 배열 크기
 18 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ ./a.out 19
 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 
 (88 / 4 = 22) # 실제 할당된 배열 크기
 19 # 기대했던 배열 크기
 (bionic)soyeomul@localhost:~/111$ uname -a
 Linux localhost 3.18.0-20388-g6bf827ea9b15 #1 SMP PREEMPT Thu Apr 23 00:57:25 PDT 2020 aarch64 aarch64 aarch64 GNU/Linux
 (bionic)soyeomul@localhost:~/111$ 

 # 참고문헌:
 [1] https://stackoverflow.com/questions/1281686/determine-size-of-dynamically-allocated-memory-in-c
 [2] https://modoocode.com/98
 [3] https://kldp.org/node/24528
 [4] https://m.blog.naver.com/tipsware/221250121797
 [5] https://m.blog.naver.com/ruvendix/220859539575
 [6] https://blockdmask.tistory.com/56
 [7] 뽀빠이님과 렉스님 -- 우분투 한국 대화방 [2020-05-26]
***/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

/* #define MSIZE(ptr) malloc_usable_size((void*)ptr) */

int main(int argc, char **argv)
{
	int size = atoi(argv[1]);

	if (size == 0) {
		printf("sorry\n");
		exit(1);
	}
	
	int *NUM;
	NUM = (int *)malloc(sizeof(int) * size);

	int i;
	for (i = 0; i < size; i++) {
		NUM[i] = i;
		printf("%d ", NUM[i]);
	}

	/* int psizeNUM = sizeof(NUM); */
	int sizeINT = sizeof(int);

	size_t msizeNUM = malloc_usable_size(NUM);
	int divN = msizeNUM / sizeINT;
	printf("\n(%lu / %d = %d) # 실제 할당된 배열 크기\n", msizeNUM, sizeINT, divN);

	/* 사용자 기대값 계산 시작 */
	int esizeNUM;
	if (NUM[divN - 1] != 0) {
		esizeNUM = divN;
	}
	else if (NUM[divN - 2] != 0) {
		esizeNUM = divN - 1;
	}
	else if (NUM[divN - 3] != 0) {
		esizeNUM = divN - 2;
	}
	else if (NUM[divN - 4] != 0) {
		esizeNUM = divN - 3;
	}
	else if (NUM[divN - 5] != 0) {
		esizeNUM = divN - 4;
	}
	else {
		esizeNUM = divN - 5;
	}
	printf("%d # 기대했던 배열 크기\n", esizeNUM);  
        /* 사용자 기대값 계산 끝 */
	
	free(NUM);
	
	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 최초 작성일: 2020년 5월 26일
 * 마지막 갱신: 2020년 5월 29일
 */
