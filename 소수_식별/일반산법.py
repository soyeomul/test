#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 소수의 정의: 1과 자신으로만 나누어지는 정수

# 소수 식별 일반 산법:
# 1과 자신 이외의 수로 나누어 떨어지면 소수가 *아님* ===> "False"

# 테스트 유효범위: 2이상의 양의 정수

import sys

def f(x):
    if x == 2:
        return x, "True"

    for i in range(2, x):
        if x % i == 0:
            return x, "False"

    return x, "True"

범위값 = int(sys.argv[1]) # 2부터 범위값까지 테스트

for 숫자 in range(2, 범위값+1):
    print(f(숫자))

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 4월 4일

# EOF
