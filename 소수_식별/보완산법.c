/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

/* 
 * 입력값을 누적 소수들로만 나누어 떨어지면 소수가 아님("False")에 착안하여 구현했으며
 * 입력값은 대략 10만까지로 한계를 설정했습니다
 * 
 * 실험결과 일반산법보다 속도가 빠릅니다
 *
 * 실험환경: ARM64 크롬북 (코드명: birch)
 * 운영체제: 우분투 18.04 LTS
 * gcc --version: gcc (Ubuntu/Linaro 7.5.0-3ubuntu1~18.04) 7.5.0
 * 
 * $ time ./a.out 100000
 * 2.408 초 (일반산법)
 * 1.038 초 (보완산법 -- 본 코드)
 */

int lp[9999] = {2,}; /* 소수 누적값 목록 */
int llp = 1; /* 소수 누적 갯수 */

int prime(int n)
{
	char F[8] = {'\'', 'F', 'a', 'l', 's' , 'e', '\'', '\0',};
	char T[7] = {'\'', 'T', 'r', 'u', 'e', '\'', '\0',};
	
	if (n == 2) {
		printf("(%d, %s)\n", n, T);
		
		return 0;
	}
	
	int i;
	
	for (i = 0; i < llp; i++) {
		if (n != lp[i] && n % lp[i] == 0) {
			printf("(%d, %s)\n", n, F);
			
			return 0;
		}
	}
	
	lp[llp] = n;
	llp = llp + 1;
	
	printf("(%d, %s)\n", n, T);
	/* printf("누적갯수: %d\n", llp); */

	return 0;
}

int main(int argc, char **argv)
{
	int i;
	int j = atoi(argv[1]);
       
	for (i = 2; i < j+1; i++) {
		prime(i);
	}

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 4월 4일
 */

/* EOF */
