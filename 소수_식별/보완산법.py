#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 소수의 정의: 1과 자신으로만 나누어지는 정수

# 소수 식별 속도 보완 산법:
# (일반산법) 1과 자신 이외의 수로 나누어 떨어지면 소수가 *아님* ===> "False"
# (보완산법) 입력값을 누적 소수들로 나누어 떨어지면 소수가 *아님* ===> "False" 

# 테스트 유효범위: 2 이상의 양의 정수

import sys

lp = 소수_누적값_목록 = []

def f(x):
    if x == 2:
        lp.append(x)
        return x, "True"
    
    for n in lp:
        if n != x and x % n == 0:
            return x, "False" # 소수 아님을 화면에 출력하고 함수 탈출

    lp.append(x) # 소수로 식별되면 [소수_누적값_목록] 갱신할것
    return x, "True" # 소수를 화면에 출력하고 함수 탈출
     
범위값 = int(sys.argv[1]) # 2부터 범위값까지 실험

for 숫자 in range(2, 범위값+1):
    print(f(숫자))

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 4월 4일

# EOF
