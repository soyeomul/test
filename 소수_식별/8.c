/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

/* #define MSIZE(ptr) malloc_usable_size((void*)ptr) */

/* 
 * 소수(Prime Numbers) 식별하는 C언어 코드입니다
 * 보완산법.c [2] 에다가 malloc(메모리 동적 할당) [1] 적용했습니다
 * 포인터/배열 개념이 어려워서 거의 한달간 공부하여 코드를 작성하고 실험했습니다 진짜 빡세더이다...
 * 저처럼 코딩 공부하는 비전공자분들께 도움이 되었음 좋겠어요^^^ 「Public Domain」입니다^^^ 
 * ^고맙습니다 감사합니다_^))//
 *
 * ### 참고문헌 ### 
 * [1] https://gitlab.com/soyeomul/test/-/blob/master/%EC%86%8C%EC%88%98_%EC%8B%9D%EB%B3%84/3.c
 * [2] https://gitlab.com/soyeomul/test/-/blob/master/%EC%86%8C%EC%88%98_%EC%8B%9D%EB%B3%84/%EB%B3%B4%EC%99%84%EC%82%B0%EB%B2%95.c
 * [3] https://modoocode.com/98
 * 
 * ### 실험 환경 ###
 * ARM64 크롬북 (코드명: birch)
 * 배포판: 우분투 18.04 LTS
 * gcc (Ubuntu/Linaro 7.5.0-3ubuntu1~18.04) 7.5.0
 * 커널: Linux localhost 3.18.0-20388-g6bf827ea9b15 #1 SMP PREEMPT Tue May 5 00:07:28 PDT 2020 aarch64 aarch64 aarch64 GNU/Linux
 */

#define MAX_LEN 9999

int *lp[MAX_LEN]; /* 소수값들이 저장되는 곳 */
int llp = 0; /* 소수 누적 갯수 */

int sizeINT = sizeof(int);
size_t tlp = 0; /* 할당된 메모리를 수첩에 적어둡니다 */

int init(int n)	
{
	/* 소수 누적값 초기화: *lp[0] = 2 */
        
	int *pn;
	pn = (int *)malloc(sizeINT * n);
	
	*pn = 2;

	lp[0] = pn;
	llp = 1;
	
        tlp = 24; /* 첫 시작 메모리 할당량: 
                   * (int *)malloc(sizeof(int) * 1)
                   * 
                   * 환경: GCC GNU/Linux (ARM64 크롬북)
                   */
	
	/* printf("%d # 출력예시\n", *lp[0]); */

	return 0;
}	

int prime(int *n)
{
	char F[8] = {'\'', 'F', 'a', 'l', 's', 'e', '\'', '\0',};
	char T[7] = {'\'', 'T', 'r', 'u', 'e', '\'', '\0',};

	if (*n == 2) {
		printf("(%d, %s)\n", *n, T);

		return 0;
	}

	int i;

	for (i = 0; i < llp; i++) {
		if (*n != *lp[i] && *n % *lp[i] == 0) {
			printf("(%d, %s)\n", *n, F);

			return 0;
		}
	}

	int *pn;
	pn = (int *)malloc(sizeINT * (llp + 1));

	size_t msize_pn = malloc_usable_size(pn);
	tlp = msize_pn; /* 할당된 메모리를 수첩에 적어둡니다 */
	
	*pn = *n;

	lp[llp] = pn;
	llp = llp + 1;

	printf("(%d, %s)\n", *n, T);

	return 0;
}

int _free(int *n) /* 메모리 설거지 */
{
	int rlp = tlp / sizeINT; /* 할당된 총 배열 크기 */
	int i;

	for (i = *n; i < rlp; i++) {
		free(lp[i]); /* 짜투리 배열 설거지 */
	}

	return 0;
}

int main(int argc, char **argv)	
{	
	init(1); /* 초기화: *lp[0] = 2; */
	
	int i;
	int j = atoi(argv[1]);

	for (i = 2; i < j+1; i++) {
		prime(&i);
	}

	int n = llp;
	_free(&n); /* 메모리 설거지 */

	if (argc > 2) { /* 디버그 모드 */
		printf("\n===>");
		
		int k;
		for (k = 0; k < llp; k++) {
			printf("\t %p -- %d \n", lp[k], *lp[k]);
		}
		
		int len_lp = tlp / sizeINT;	
		printf("\nDEBUG: [마지막소수: %d, 누적갯수: %d, 배열크기: %d, 입력값: %d]\n", \
		       *lp[llp-1], llp, len_lp, j);
	}

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 최초 작성일: 2020년 5월 31일
 * 마지막 갱신: 2021년 2월 2일
 */
