// getenv()
//
// 참고문헌:
// <https://mingrammer.com/gobyexample/environment-variables/>

package main

import "os"
import "fmt"

func main() {
	os.Setenv("GOPHER", "soyeomul")
	s := os.Getenv("GOPHER")

	fmt.Println("GOPHER:", s)
	fmt.Println(os.Getenv("PATH"))
}

// 편집: GNU Emacs 27.1 (Debian Bullseye 11)
// 마지막 갱신: 2021년 10월 16일
