#include <stdio.h>

int main()
{
	char c = 'a';
	printf("variable c = %c \n", c);

	char *ptr = &c;

	*ptr = 'b';

	printf("variable c = %c \n", c);

	return 0;
}
