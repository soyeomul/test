#!/usr/bin/python3
# -*- coding: utf-8 -*-

f = open("in.log", "r")

data = f.read()

data_s = data.split()

data_2 = data_s[2]
data_4 = data_s[4]

print("Patch", data_2, data_4[1:-1])

f.close()

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 3월 13일

"""
$ cat in.log
Error: Patch AT_6.2.0.b_0009c expected 'ASML-BB-001-0017A-HW' to be at version 0.201703311623
"""
