#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# KLDP [163338]

a = list(range(36)) # 사슴 초기화 
b = list(range(36)) # 학 초기화
 
def hap(x, y): # 합
    xx = []; yy = []
    for i in x:
        for j in y:
            if i + j == 35:
                xx.append(i)
                yy.append(j)
 
    return zip(xx, yy)
 
def hapdari(x, y): # 합다리
    xx = []; yy = []
    for i in x:
        for j in y:
            if 4 * i + 2 * j == 94:
                xx.append(i)
                yy.append(j)
 
    return zip(xx, yy)
 
합 = list(hap(a, b))
합다리 = list(hapdari(a, b))
 
print(set(합) & set(합다리), "앞에꺼 [사슴] 뒤에꺼 [학] 입니다") 
 
# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 5월 24일
