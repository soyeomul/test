/* -*- coding: utf-8 -*- */

/* KLDP 163338 */

#include <stdio.h>
#include <stdlib.h>

/*** 
 ### 우분투 18.04 LTS 에서 실험했습니다 ###
 (bionic)soyeomul@localhost:~/gitlab/test/사슴_학$ gcc 사슴_학.c
 (bionic)soyeomul@localhost:~/gitlab/test/사슴_학$ ./a.out 0 > 0
 (bionic)soyeomul@localhost:~/gitlab/test/사슴_학$ ./a.out 1 > 1
 (bionic)soyeomul@localhost:~/gitlab/test/사슴_학$ grep -f 0 1
 (12, 23)
 (bionic)soyeomul@localhost:~/gitlab/test/사슴_학$ 
***/

int SSM[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, \
	     10, 11, 12, 13, 14, 15, 16, 17, 18, 19, \
	     20, 21, 22, 23, 24, 25, 26, 27, 28, 29, \
	     30, 31, 32, 33, 34, 35,};

int HAK[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, \
	     10, 11, 12, 13, 14, 15, 16, 17, 18, 19, \
	     20, 21, 22, 23, 24, 25, 26, 27, 28, 29, \
	     30, 31, 32, 33, 34, 35,};

int idxSSM;
int sizeSSM = sizeof(SSM) / sizeof(int);

int idxHAK;
int sizeHAK = sizeof(HAK) / sizeof(int);

/* int sum_hap = SSM[idxSSM](int) + HAK[idxHAK](int); */ 
/* int sum_hapdari = 4 * SSM[idxSSM](int) + 2 * HAK[idxHAK](int); */ 

int hap(idxSSM, idxHAK)
{
	for (idxSSM = 0; idxSSM < sizeSSM; idxSSM++) {
		for (idxHAK = 0; idxHAK < sizeHAK; idxHAK++) {
			if (SSM[idxSSM] + HAK[idxHAK] == 35) {
				printf("(%d, %d)\n", SSM[idxSSM], HAK[idxHAK]);
			}
		}
	}

	return 0;
}

int hapdari(idxSSM, idxHAK)
{
	for (idxSSM = 0; idxSSM < sizeSSM; idxSSM++) {
		for (idxHAK = 0; idxHAK < sizeHAK; idxHAK++) {
			if (4 * SSM[idxSSM] + 2 * HAK[idxHAK] == 94) {
				printf("(%d, %d)\n", SSM[idxSSM], HAK[idxHAK]);
			}
		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	if (argc == 1) {
	        printf("0 ===> 합, 1 ===> 합다리\n");

		return 0;
	}

	int choice = atoi(argv[1]);
	
	if (choice == 0) {
		hap(idxSSM, idxHAK);

		return 0;
	}
	else if (choice == 1) {
		hapdari(idxSSM, idxHAK);

		return 0;
	}
	else {
		printf("0 ===> 합, 1 ===> 합다리\n");
		
		return 0;
	}

	return 0;
}
      
/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 5월 24일
 */
