#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 파이썬코드의 탭("\t")을 스페이스(" ") 4개로 변환합니다
#

"""
- 현재 디렉토리에서만 지원합니다
- 또한 sys.argv 인수에 파일 *하나*만 지원합니다
- 그래서 가능한한 들여쓰기 짬뽕으로 인한 에러가 난 코드에만 쓰도록 합니다
"""

import sys

def 변환(xyz):
    rf = open(xyz, "r")
    data = rf.read()
    rf.close()

    data_new = data.replace("\t", " "*4)

    wf = open(xyz, "w")
    wf.write(data_new)
    wf.close()

    if True:
        print("{0} 변환성공!".format(xyz))

if __name__ == "__main__":
    from subprocess import call
    
    _file = sys.argv[1]
    _file_bak = "{0}.{1}".format(_file, "orig")
    """변환전 백업파일을 미리 만듭니다"""
    call("cp -a {0} {1}".format(_file, _file_bak), shell=True)
    
    변환(_file)

# 편집: Gnus Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 1월 3일
