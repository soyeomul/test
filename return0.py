# -*- coding: utf-8 -*-

# 파이썬의 "return 0" 은 C 언어와는 그 의미가 다르다

def f():
    return 0

g = f()

print(repr(g), type(g)) # 0 <class 'int'>

# 편집: Emacs 26.2 (Ubuntu 18.04)
