# -*- coding: utf-8 -*-

"""
100까지의 자연수 중에서,
8의 배수지만 12의 배수는 아닌걸
출력하시오.
"""

temp = []
for i in range(1, 101):
    if i % 8 == 0 and i % 12 != 0:
        temp.append(i)

print(temp)

# 테스트: 우분투 18.04, 파이썬 3.6.7

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 4일
